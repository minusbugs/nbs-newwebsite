<!DOCTYPE html>
<html>
	<head>
		<title>Naipunnya Business School</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/responsive.css" rel="stylesheet" type="text/css" />
		
		
		<link href="css/template.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,400italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Dosis:400,300,500,600,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- Magnific Popup core CSS file -->
		<link rel="stylesheet" href="components/magnific-popup/magnific-popup.css">
		<link rel="stylesheet" type="text/css" href="components/owl-carousel/owl.carousel.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="components/owl-carousel/owl.transitions.css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" />
	</head>
	<body>
		<div class="container-fluid">
<div class="row topnav">
	
	<div class="col-md-6">
		<p style=""><i class="fa fa-phone"></i><span class="padding-left-right-10">Call Us at +(91) 960 507 8601 </span> <i class="fa fa-envelope"></i><span class="padding-left-right-10">mail@mbanimit.ac.in</span> </p>
		
	</div>
	<div class="col-md-4">
		<nav class="secondary-menu pull-right" style="margin-top: 5px;">
					<ul class="nav nav-pills">
						<li class="menu-item-has-children">
							<a href="mailto:mail@mbanimit.ac.in?subject=Carrer Link">Career</a>
						</li>
						<li class="menu-item-has-children">
							<a href="https://naipunnya.almaconnect.com/">Alumni </a>
							
						</li>
						<li class="menu-item-has-children">
							<a href="GRIEVANCE.php">Grievance</a>
							
						</li>
						
						
					</ul>
				</nav>
			
	
	</div>
	<div class="col-md-2">
		<div class="social-icons " style="margin-top: 5px;">
			<a href="https://twitter.com/NaipunnyaS" class="social-icon"><i class="fa fa-twitter has-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"></i></a>

			<a href="https://www.facebook.com/mbanimit" class="social-icon"><i class="fa fa-facebook has-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"></i></a>

			<a href="#" class="social-icon"><i class="fa fa-google-plus has-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="GooglePlus"></i></a>

			<a href="#" class="social-icon"><i class="fa fa-linkedin has-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn"></i></a>
		
			
		</div>
	</div>
	
	
</div>
<div class="row blue-menu">
	<div class="logo-nbs">
		<img src="images/nbs_logo.png" alt="">
	</div>
	<div class="header-inner">
		<nav id="nav">
			<ul class="header-top-nav">
				<li class="visible-mobile">
					<a href="#mobile-nav-wrapper" data-toggle="collapse"><i class="fa fa-bars has-circle"></i></a>
				</li>
			</ul>
			<ul id="main-nav" class="hidden-mobile">
				<li class="menu-item-has-children">
					<a href="index.php">Home</a>
				</li>
				<li class="menu-item-has-children">
					<a href="about.php">ABOUT </a>
					<ul class="sub-nav position-left">
						<li><a href="about.php">History</a></li>
						<li><a href="about.php#nbs-management">Management</a></li>
						<li><a href="about.php#nbs-vison">Vision & Mission</a></li>
						<li><a href="about.php#nbs-advisory">Advisory board</a></li>
						<li><a href="ExecutiveDirMessage.php">Message</a></li>
						<li><a href="Archdiocese.php">Ekm – Angamaly Archdiocese</a></li>
						
					</ul>
				</li>
				<li class="menu-item-has-children">
					<a href="#">ACADEMICS</a>
					<ul class="sub-nav position-left">
						<li><a href="course.php">Course : MBA</a></li>
						<li><a href="admission.php">Admission & Eligibility</a></li>
						<li><a href="Feestructure.php">Fees Structure</a></li>
						<li><a href="Curriculum.php">Curriculum</a></li>
						<li><a href="Infrastructure.php">Infrastructure/Facilities</a></li>
						<li><a href="Project.php">Student Project</a></li>
						
					</ul>
				</li>
				<li class="menu-item-has-children">
					<a href="#">FACULTY</a>
					<ul class="sub-nav position-left">
						<li><a href="Faculty.php">Faculty Team</a></li>
						<li><a href="research.php">Research &  Paper Publications </a></li>
						
						
					</ul>
				</li>
				
				<li class="menu-item-has-children">
					<a href="Events.php">EVENTS</a>
				</li>
				<li class="menu-item-has-children">
					<a href="Gallery.php">GALLERY</a>
				</li>
				
				<li class="menu-item-has-children">
					<a href="placement.php">PLACEMENT </a>
					<ul class="sub-nav position-left">
						<li><a href="placement.php#placed">Recent Placements</a></li>
						<li><a href="placement.php#internship">Internship</a></li>
						<li><a href="Attachments/MBA-Placement-Brochure.pdf">View Brochure</a></li>
						
						
					</ul>
				</li>
				<li class="menu-item-has-children">
					<a href="contact.php">CONTACT US</a>
				</li>
				
			</ul>
		</nav>
	</div>
</div>
<div class="mobile-nav-wrapper collapse visible-mobile" id="mobile-nav-wrapper">
	<ul class="mobile-nav">
		<li class="menu-item-has-children">
			<a href="index.html">Home</a>
		</li>
		<li class="menu-item-has-children">
			<span class="open-subnav"></span>
			<a href="about.html">ABOUT</a>
			<ul class="sub-nav">
				<li><a href="#">History</a></li>
				<li><a href="#">Management</a></li>
				<li><a href="#">Scope Vision Mission</a></li>
				<li><a href="#">Advisory board</a></li>
				<li><a href="#">Executive Director message</a></li>
				<li><a href="#">Ekm – Angamaly Archdiocese</a></li>
			</ul>
		</li>
		<li class="menu-item-has-children">
			<span class="open-subnav"></span>
			<a href="#">ACADEMICS</a>
			<ul class="sub-nav">
				<li><a href="#">Course : MBA</a></li>
				<li><a href="#">Admission & Eligibility</a></li>
				<li><a href="#">Fees Structure</a></li>
				<li><a href="#">Curriculum</a></li>
				<li><a href="#">Infrastructure</a></li>
				<li><a href="#">Students Project / Internship</a></li>
			</ul>
		</li>
		<li class="menu-item-has-children">
			<a href="#">FACULTY</a>
			<ul class="sub-nav position-left">
				<li><a href="Faculty.php">Faculty Team</a></li>
				<li><a href="#">Reasearch And Paper </a></li>
				
				
			</ul>
		</li>
		<li class="menu-item-has-children">
			<a href="Events.php">EVENTS</a>
		</li>
		<li class="menu-item-has-children">
			<a href="Gallery.html">GALLERY</a>
		</li>
		<li class="menu-item-has-children">
			<a href="#">PLACEMENT</a>
		</li>
		<li class="menu-item-has-children">
			<a href="contact.php">CONTACT US </a>
		</li>
	</ul>
</div>