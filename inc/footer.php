
<footer id="footer" class="style3">

	        <div class="footer-wrapper">
                <div class="container">
                    <div class="row add-clearfix same-height">

                    	  <div class="col-sm-6 col-md-4">
                            <h5 class="section-title box">Naipunnya group of Institutions</h5>
                          <!--   <p>Naipunnya group of Institutions is a project of the Archdiocese of Ernakulam-Angamaly. Naipunnya was launched back in 1998 under the visionary leadership of Rev.Fr.Sebastian Kalapurakal,the first Director of Naipunnya. Today, Naipunnya has spread its wings, with a College at Cherthala in the district of Alleppey and Public Schools at Edakkunnu and Thrikkakara. Naipunnya also has has stepped into pre-examination training for Civil Service Examination (IAS/IPS Exam) and Overseas Education Consultancy. Our group institutions are: </p> -->
                            <ul class="arrow useful-links">
                                <li><a href="http://www.naipunnya.ac.in/">Naipunnya Institute of Management and Information &nbsp;&nbsp;&nbsp;&nbsp;Technology, Pongam</a></li>
                                <li><a href=" http://naipunnya.in/">Naipunnya School of Management,Cherthala</a></li>
                                <li><a href=" http://www.naipunnyaint.com/">Naipunnya International,Eranakulam</a></li>
                                <li><a href="https://www.naipunnya.edu.in/">Naipunnya Public School,Karukutty,Ernakulam </a></li>
                                <li><a href=" https://naipunnya.org.in/">Naipunnya Public School,Kakkanad, Kochi</a></li>
                                <li><a href="http://www.naipunnyawelfareservices.com/">Naipunnya Welfare Services,Karukutty</a></li>
                            </ul>
                        </div>

                        <div class="col-sm-6 col-md-4">
                            <h5 class="section-title box">Recent News</h5>
                            <ul class="recent-posts">
                            	<?php
								include("db/dbConn.php");
								$sql = "SELECT * FROM events";
								$result = $conn->query($sql);
								
								while($row = $result->fetch_assoc()) {
									
								?>
                                <li>
                                  
                                    <div class="post-content">
                                        <a href="#" class="post-title"><?php echo $row["EventName"]; ?></a>
                                        <p class="post-meta" style="color: #8c8c8c;"><?php echo $row["EventDescription"]; ?></p>
                                    </div>
                                </li>
                              <?php
                          }?>
                              
                            </ul>
                        </div>
                        
                      
                        <div class="col-sm-6 col-md-4">
                            <h5 class="section-title box">About NBS</h5>
                            Naipunnya Business School (NBS) is an offshoot of Naipunnya Institute of Management and Information Technology (NIMIT), Pongam, Koratty. NIMIT is a project of the Archdiocese of Ernakulam-Angamaly .NIMIT was launched way back in 1998 under the visionary leadership of Rev. Fr. Sebastian Kalapurackal, the founder director of Naipunnya
                            <p><a style="color: #18a5f7 !important;" href="about.php">Read More</a></p>

                            <div class="social-icons">
                                <a href="https://twitter.com/NaipunnyaS" class="social-icon"><i class="fa fa-twitter has-circle" data-toggle="tooltip" data-placement="top" title="Twitter"></i></a>
                                <a href="https://www.facebook.com/mbanimit" class="social-icon"><i class="fa fa-facebook has-circle" data-toggle="tooltip" data-placement="top" title="Facebook"></i></a>
                                <a href="https://www.instagram.com/mbanbs/" class="social-icon"><i class="fa fa-instagram has-circle" data-toggle="tooltip" data-placement="top" title="GooglePlus"></i></a>
                                
                               
                            </div>
                            <a href="contact.php" class="btn btn-sm style4">Contact Us</a>
                           
                            <a href="#" id="toTop" class="back-to-top"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
	
	<div class="footer-bottom-area">
		<div class="container">
			<div class="copyright-area">
				<nav class="secondary-menu">
					<ul class="nav nav-pills">
						<li class="menu-item-has-children">
							<a href="index.php">Home</a>
						</li>
						<li class="menu-item-has-children">
							<a href="about.php">ABOUT </a>
							
						</li>
						
						<li class="menu-item-has-children">
							<a href="Faculty.php">FACULTY</a>
							
						</li>
						
						<li class="menu-item-has-children">
							<a href="Events.php">EVENTS</a>
						</li>
						<li class="menu-item-has-children">
							<a href="Gallery.php">GALLERY</a>
						</li>
						
						<li class="menu-item-has-children">
							<a href="contact.php">CONTACT US</a>
						</li>
						
					</ul>
				</nav>
				<div class="copyright">
					&copy; 2018 <em>by</em> <a href="http://minusbugs.com/">minusbugs.com</a>
				</div>
			</div>
		</div>
	</div>
</footer>
