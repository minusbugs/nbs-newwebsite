
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Life@NBS</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">Life @ NBS </li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<section id="content">
			<div class="container">
				<div class="row">
					
					<div id="main">
						 <div class="blog-posts layout-timeline layout-fullwidth">
						 	
                        <div class="timeline-author">
                            <img src="images/nbs.png" alt="">
                        </div>
                        <div class="iso-container iso-col-2 style-masonry has-column-width">
                             <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Vihaan</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/vihaan1.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Vihaan 2017 (Vidyarambham)</a></h3>
                                        <p>(Vidyarambham): The Vidyarambham ceremony for first year MBA students
was conducted on 18.07.2017. Mr. M. P. Joseph  IAS, was chief guest.</p>
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Orientation Program</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/vihaan.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Orientation Program</a></h3>
                                        <!-- <p>(Vidyarambham): The Vidyarambham ceremony for first year MBA students
was conducted on 18.07.2017. Mr. M. P. Joseph  IAS, was chief guest.</p> -->
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">CAZADORZ</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/CAZADORZ1.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">CAZADORZ</a></h3>
                                       
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Aazadi </div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/Aazadi.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Aazadi </a></h3>
                                       
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Placement drives  </div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/placement1.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Placement drives  </a></h3>
                                       
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                             <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">International Yoga Day  </div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/yoga.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">International Yoga Day </a></h3>
                                       
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                             <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Industry Interaction   </div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/industry.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Industry Interaction </a></h3>
                                       
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>



                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Aagneya</div>
                                    <div class="soap-gallery style2 thumbnail-full">
                                         <a class="sgImg image" href="images/lifenbs/aagneya_3.jpg" data-thumb="
                                            images/lifenbs/aagneya_3.jpg">
                                            <img src="images/lifenbs/aagneya_3.jpg" alt="">
                                            <!-- <div class="image-extras"></div> -->
                                        </a>

                                        <a class="sgImg image" href="images/lifenbs/aagneya_1.jpg" data-thumb="
                                            images/lifenbs/aagneya_1.jpg">
                                            <img src="images/lifenbs/aagneya_1.jpg" alt="">
                                            <!-- <div class="image-extras"></div> -->
                                        </a>
                                        <a class="sgImg image" href="images/lifenbs/aagneya_2.jpg" data-thumb="
                                            images/lifenbs/aagneya_2.jpg">
                                            <img src="images/lifenbs/aagneya_2.jpg" alt="">
                                            <!-- <div class="image-extras"></div> -->
                                        </a>
                                       
                                       
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Aagneya</a></h3>
                                        <p>A one day Intra-Management fest was held to bring out the managerial skills of our students.</p>
                                    </div>
                                    <div class="post-action">
                                       
                                    </div>
                                </article>
                            </div>
                            

                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Outbound Training</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/outbond.jpg" alt="">
                                         
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Outbound Training</a></h3>
                                         <p>MBA student were sent for a confidence building exercises by Kalypso adventures where there were trained on mountain climbing, Rappelling, River crossing, and night trekking using communication and mapping devices.</p>
                                    </div>
                                    <div class="post-action">
                                     
                                    </div>
                                </article>
                            </div>
                             <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Donation Camp</div>

                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Blood Donation Camp</a></h3>
                                        <p>MBA students with SWAS organized a blood donation camp.</p>
                                    </div>
                                    <div class="post-action">
                                      
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Hills Campaign</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/hilcamp.jpg" alt="">
                                         
                                        </div>
                                    </div>
                                    
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Clean The Hills Campaign</a></h3>
                                        <p>MBA students participated in the clean the hills campaign at Devikulam.</p>
                                    </div>
                                    <div class="post-action">
                                      
                                    </div>
                                </article>
                            </div>
                           
                          
                          <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">AVANI</div>
                                    <div class="soap-gallery style2 thumbnail-full">
                                        <a class="sgImg image" href="images/lifenbs/avani_1.jpg" data-thumb="
                                            images/lifenbs/avani200_1.jpg">
                                            <img src="images/lifenbs/avani_1.jpg" alt="">
                                            <!-- <div class="image-extras"></div> -->
                                        </a>
                                        <a class="sgImg image" href="images/lifenbs/avani_2.jpg" data-thumb="
                                            images/lifenbs/avani200_2.jpg">
                                            <img src="images/lifenbs/avani_2.jpg" alt="">
                                            <!-- <div class="image-extras"></div> -->
                                        </a>
                                        <a class="sgImg image" href="images/lifenbs/avani_3.jpg" data-thumb="
                                            images/lifenbs/avani200_3.jpg">
                                            <img src="images/lifenbs/avani_3.jpg" alt="">
                                            <!-- <div class="image-extras"></div> -->
                                        </a>
                                       
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">AVANI</a></h3>
                                        <p>Onam celebration celebrated in the campus with ‘Onasadhya’.</p>
                                    </div>
                                    <div class="post-action">
                                       
                                    </div>
                                </article>
                            </div>
                           
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Swatch Bharat</div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Swatch Bharat Abhiyan</a></h3>
                                        <p>As part of the Swatch Bharat Abhiyan our students cleaned the campus on 15th August</p>
                                    </div>
                                    <div class="post-action">
                                      
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Placement Training</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/placement.jpg" alt="">
                                         
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Placement Training</a></h3>
                                        <p>Placement training was held on to train students on attending interviews and GD’s by Mr. Rajesh from YEZ associates</p>
                                    </div>
                                    <div class="post-action">
                                      
                                    </div>
                                </article>
                            </div>
                             <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Keraleeyam</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/keraliyam.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Keraleeyam-2017 </a></h3>
                                        <p>NBS Celebrated "Keraleeyam-2017 with interdepartmental competitions for UG/PG students.</p>
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Charity program</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/orphan.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Christmas lunch to the children of Nazareth orphanage </a></h3>
                                        <p>Students of NBS offered a Christmas lunch to the children of Nazareth orphanage on 22nd December, 2017</p>
                                    </div>
                                    <div class="post-action">
                                      
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Brochure Release</div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Placement Brochure Release </a></h3>
                                        <p>Placement brochure release in the month of September, 2016 by Dr. Benny, Joint MD, Arjuna Extracts Ltd.</p>
                                    </div>
                                    <div class="post-action">
                                      
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Vaidakthya</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/vaidakthya.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Vaidakthya </a></h3>
                                        <p>Vaidakthya – 2017, National conference (20th May, 2017).</p>
                                    </div>
                                    <div class="post-action">
                                      
                                    </div>
                                </article>
                            </div>
                            <div class="iso-item">
                                <article class="post post-masonry">
                                    <div class="post-date">Indictio</div>
                                    <div class="post-image">
                                        <div class="image">
                                            <img src="images/lifenbs/Indictio.jpg" alt="">
                                            
                                        </div>
                                    </div>
                                    <div class="post-content no-author-img">
                                        <div class="post-meta">
                                           
                                        </div>
                                        <h3 class="entry-title"><a href="#">Indictio 2017 </a></h3>
                                        <p>Convocation, Former Vice Chancellor of Adhishankara university was the  chief guest.</p>
                                    </div>
                                    <div class="post-action">
                                    
                                    </div>
                                </article>
                            </div>
                        </div>
                        <a href="#" class="load-more"><i class="fa fa-angle-double-down"></i></a>
                    
						 </div>
					</div>
				</div>
			</div>
			
		</section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
          <!-- carouFredSel -->
    <script type="text/javascript" src="components/carouFredSel-6.2.1/jquery.carouFredSel-6.2.1.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>