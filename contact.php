
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			
			
		</div>
		<!-- HEADER -->
		
		<div class="soap-google-map"></div>
		<!-- CONTENT-Features -->
		<div class="single-post">
			<section id="content">
				<div class="container">
					<div id="main">
						<section id="content">
							<div class="section container">
								<div class="row">
									<div class="col-sm-8 box">
										<form>
											<div class="row">
												<div class="form-group col-sms-6 col-sm-6">
													<input type="text" class="input-text full-width" placeholder="First name">
												</div>
												<div class="form-group col-sms-6 col-sm-6">
													<input type="text" class="input-text full-width" placeholder="Last name">
												</div>
												<div class="form-group col-sms-6 col-sm-6">
													<input type="text" class="input-text full-width" placeholder="Email address">
												</div>
												<div class="form-group col-sms-6 col-sm-6">
													<input type="text" class="input-text full-width" placeholder="Website">
												</div>
											</div>
											<div class="form-group">
												<textarea rows="10" class="input-text full-width" placeholder="Send Message"></textarea>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-md style1">Write Message</button>
											</div>
										</form>
									</div>
									<div class="col-sm-4">
										<ul class="contact-address style1">
											<li class="office-address">
												<i class="fa fa-map-marker"></i>
												<div class="details">
													<h5>Naipunnya Business School</h5>
													<p>Pongam, Koratty East, Thrissur District </p>
													<p>Kerala State, INDIA. Pin - 680 308</p>
												</div>
											</li>
											<li class="phone">
												<i class="fa fa-phone"></i>
												<div class="details">
													<h5>Phone</h5>
													<p>
														Local: +91 480 2631243
														<br>
														Mobile: +(91) 960 507 8601
													</p>
												</div>
											</li>
											<li class="email-address">
												<i class="fa fa-envelope"></i>
												<div class="details">
													<h5>Office Address</h5>
													<p>
														mail@mbanimit.ac.in
														<br>
														www.nbs.ac.in
													</p>
												</div>
											</li>
										</ul>
										<div class="social-icons style1 size-md">
											<a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter has-circle"></i></a>
											<a href=")https://www.facebook.com/mbanimit" class="social-icon" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook has-circle"></i></a>
											<a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="GooglePlus"><i class="fa fa-google-plus has-circle"></i></a>
											<a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin has-circle"></i></a>
											
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section>
		</div>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<!-- Google Map Api -->
		<script type='text/javascript' src="http://maps.google.com/maps/api/js?key=AIzaSyDIT71Xztj6ZPqnWBYGyplS88QfUO1EIO8&sensor=false&amp;language=en"></script>
		<script type="text/javascript" src="js/gmap3.js"></script>
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
		<script type="text/javascript">
        sjq(".soap-google-map").gmap3({
            map: {
                options: {
                    center: [10.2463667,76.3646792],
                    zoom: 14,
					mapTypeControlOptions: {
						position: google.maps.ControlPosition.RIGHT_BOTTOM
					},
					zoomControlOptions: {
						position: google.maps.ControlPosition.LEFT_CENTER
					},
					panControlOptions: {
						position: google.maps.ControlPosition.LEFT_CENTER
					}
                }
            },
            marker:{
                values: [
                    {latLng:[10.246622,76.372773], data:"nbs"}

                ],
                options: {
                    draggable: false,
                    // icon: "images/icon/marker.png",
                },
            }
            
        });
    </script>
	</body>
</html>