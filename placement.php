<?php include("db/dbConn.php"); ?>
<style type="text/css">
	.iso-item .entry-title{
		padding-top:35px;
	}
</style>
<?php include("inc/side-menu.php"); ?>
<!-- BANNER -SLIDER -->
<div class="page-title-container row">
	<div class="page-title">
		<div class="container">
			<h1 class="entry-title">Placement</h1>
		</div>
	</div>
	<ul class="breadcrumbs">
		<li><a href="index.php">Home</a></li>
		<li class="active">Placement</li>
	</ul>
</div>

</div>
<!-- HEADER -->


<!-- CONTENT-Features -->
<section id="content">
<div class="container">
	<div class="row">
		
		<div id="main">
			<h3 class="section-title" style="font-weight:600" id="internship">Placement Details</h3>
			<div class="iso-container iso-col-3 style-masonry has-column-width blog-posts">
				<?php
				
				$sql = "SELECT * FROM placement where PlaceMentType=1";
				$result = $conn->query($sql);
				
				while($row = $result->fetch_assoc()) { 
						
					?>
				<div class="iso-item">
					<article class="post post-masonry">
						<div class="post-image">
							<div class="image">
								<img alt="" src="http://admin.nbs.ac.in/assets/Placement/<?php echo $row["CompanyImageUrl"]; ?>">
								<!--  <div class="image-extras">
									<a class="post-gallery" href="#"></a>
								</div> -->
							</div>
						</div>
						<div class="post-content">
							<div class="post-author">
								<a href="#"><img alt="" src="http://admin.nbs.ac.in/assets/Placement/<?php echo $row["CandidateImageUrl"]; ?>"></a>
							</div>
							<div class="post-meta">
								
							</div>
							<h3 class="entry-title"><a href="#"><?php echo $row["CandidateName"]; ?></a></h3>
							<p><?php echo $row["Description"]; ?></p>
						</div>
						<div class="post-action">
							
						</div>
					</article>
					
				</div>
				<?php
			}
			?>
				
				
			</div>
			<h3 class="section-title" style="margin-top:10px;font-weight:600" id="internship">Internship Details</h3>
			<div class="iso-container iso-col-3 style-masonry has-column-width blog-posts">
				
				<?php
				
				$sql = "SELECT * FROM placement where PlaceMentType=2";
				$result = $conn->query($sql);
				
				while($row = $result->fetch_assoc()) { 
						
					?>
				<div class="iso-item">
					<article class="post post-masonry">
						<div class="post-image">
							<div class="image">
								<img alt="" src="http://admin.nbs.ac.in/assets/Placement/<?php echo $row["CompanyImageUrl"]; ?>">
								<!--  <div class="image-extras">
									<a class="post-gallery" href="#"></a>
								</div> -->
							</div>
						</div>
						<div class="post-content">
							<div class="post-author">
								<a href="#"><img alt="" src="http://admin.nbs.ac.in/assets/Placement/<?php echo $row["CandidateImageUrl"]; ?>"></a>
							</div>
							<div class="post-meta">
								
							</div>
							<h3 class="entry-title"><a href="#"><?php echo $row["CandidateName"]; ?></a></h3>
							<p><?php echo $row["Description"]; ?></p>
						</div>
						<div class="post-action">
							
						</div>
					</article>
					
				</div>
				<?php
			}
			?>
				
				
			
			</div>
		</div>
	</div>
</div>

</section>


<!--FOOTER-->
<?php include("inc/footer.php") ?>
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery.noconflict.js"></script>
<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Magnific Popup core JS file -->
<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- parallax -->
<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
<!-- waypoint -->
<script type="text/javascript" src="js/waypoints.min.js"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
<!-- load revolution slider scripts -->
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- plugins -->
<script type="text/javascript" src="js/jquery.plugins.js"></script>
<!-- load page Javascript -->
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/revolution-slider.js"></script>
</body>
</html>