
            <?php include("inc/side-menu.php"); ?>
            <!-- BANNER -SLIDER -->
            <div class="page-title-container row">
                <div class="page-title">
                    <div class="container">
                        <h1 class="entry-title">ENQUIRY FORM</h1>
                    </div>
                </div>
                <ul class="breadcrumbs">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Enquiry</li>
                </ul>
            </div>
            
        </div>
        <!-- HEADER -->
        
        
        <!-- CONTENT-Features -->
        <section id="content">
            <div class="container">
                
                
                <div id="main">
                    <div class="section-info" id="div_enq">
                        <div class="col-sm-12 box">
                            <!-- <form> -->
                            <div class="row">
                                
                                
                                <div class="col-md-6">
                                    <h3>Personal Deatils</h3>
                                    <div class="row">
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtFirstName" placeholder="First name">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtLastName" placeholder="Last name">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtEmail" placeholder="Email address">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtPhoneNo" placeholder="Phone No">
                                        </div>
                                        <div class="form-group col-sms-12 col-sm-12">
                                            <input type="text" class="input-text full-width" id="txtReligion" placeholder="Religion/Community">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea rows="3" class="input-text full-width" id="txtAddress" placeholder="Address for Communication"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>Educational Details</h3>
                                    <div class="row">
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtSslcMark" placeholder="% of SSLC Mark">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtSslcSchool" placeholder="SSLC School Name">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtPlusTwoMark" placeholder="% of Plus Two Mark ">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtPlusTwoSchoolName" placeholder="Plus Two School Name">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtUgMark" placeholder="% of UG">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtUgCollegeName" placeholder="College Name">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtPgMark" placeholder="% of PG">
                                        </div>
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtPgCollegeName" placeholder="College Name">
                                        </div>
                                    </div>
                                </div>
                                <!-- <hr class="color-light col-sm-8"> -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <select class="input-text full-width" id="cmdIsKmtc">
                                                <option value="No">Whether KMAT/CAT/CMAT Qualified</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group col-sms-6 col-sm-6">
                                            <input type="text" class="input-text full-width" id="txtScoreObtain" placeholder="Score Obtained">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-md style1" id="btnSave">Register</button>
                            </div>
                            <!-- </form> -->
                        </div>
                    </div>
                    <div class="section-info" id="div_sucess"  style="display:none;">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                <h1 style="color:white">Enquiry details submitted successfully</h1>
                                <p>your enquiry submitted successfully.nbs will reach you soon.</p>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
            
        </section>
        
        
        <!--FOOTER-->
        <?php include("inc/footer.php") ?>
        <!-- Javascript -->
        <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="js/nbs.js"></script>
        
        
    </body>
</html>