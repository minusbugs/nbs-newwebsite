
			<?php include("inc/side-menu.php"); ?>
			
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Infrastructure</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					
					<li class="active">Infrastructure/Facilities</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		
		
		<section id="content">
			<div class="container">
				<div class="main">
					 <div class="blog-posts">
					 	<article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/12.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Location</a></h3>
                                   
                                   <p>Naipunnya Business School (NBS) is strategically located on the NH47 in Pongam, Koratty east, the border of Thrissur district.</p>
                                   
                                </div>
                            </article>
                            <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/14.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Affiliations & Accreditations</a></h3>
                                   
                                   <p>NBS is affiliated to the Calicut University and accredited by AICTE. NBS is also certified with ISO 9001:2008.</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/3.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Campus</a></h3>
                                   
                                   <p>NBS has a sprawling natural environment, which offers academic development in communion with nature.</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/4.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Class rooms</a></h3>
                                   
                                   <p>The School is equipped with state-of-the-art infrastructural facilities, which combines the best of technology with modern facilities. The campus is Wi-Fi enabled. The institute has airy, well-ventilated class rooms with spacious and comfortable seating facilities. All modern audio-visual teaching aids like Audio Visual Aids, LCD Projector, TV etc. are employed while taking classes so as to make the classes more productive, informative and interesting.</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/5.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Library</a></h3>
                                   
                                   <p>The learning resource center is well equipped with the latest books, journals and subscriptions. While offering a host of books related to the subjects being taught, the library also has recreational materials. The Institute has a spacious reading room and a collection of the best books pertaining to the fields of food and beverage service, cooking, operations, management, engineering, facility planning, event planning, law, commerce, encyclopedias, and accounts. A variety of audio, video and computer materials are available for student, faculty and staff use</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/6.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Computer Lab</a></h3>
                                   
                                  <p>The Computer Centre which has an elaborate network of 60 workstations is coupled with a 100 MBPS internet connection.</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/14.jpg" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Cafeteria</a></h3>
                                   
                                  <p>Students can catch up with their friends at the coffee shop or enjoy a wholesome meal at the cafeteria. Measures are undertaken to ensure that the food is nutritious, while being delicious and being prepared in the most hygienic conditions. The cafeteria offers exact replicas of restaurants in hotels creating a simulated effect with furniture and fixtures. The cafeteria provides respite from the day's hectic schedule. It is a zone of informal interactions.</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/8.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Language Lab</a></h3>
                                   
                                   <p>Communication plays a pivotal role in today's business environment. Recognizing this, a Communication lab has been set up which extends special courses in areas of organizational behavior, communications, individual and group behavior, presentation skill and interview training. Naipunnya has a well-equipped language lab to enhance the communication skills of students thereby helping them to boldly face the professional world. The Language Lab has internet facility and is used as an aid in language teaching as well as for training students on communication skills. This very useful for students to learn English effortlessly and communicate eloquently.</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/1.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Auditorium</a></h3>
                                   
                                  <p>The auditorium with a seating facility for 600 people is at the heart of all functions in the Naipunnya, Pongam campus. On Mondays, it serves as the assembly hall where students conduct the choir, prayer and news analysis. During examinations, it doubles up as an examination hall too. </p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/10.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Seminar hall</a></h3>
                                   
                                   <p>The college has a seminar hall's which is aesthetically designed where all the important workshops and seminars are held. The seminar hall is facilitated with the latest technology and is well–equipped with multimedia projectors. It can accommodate up to 350 students and interactive sessions are taken up by the management, faculty, staff & students. With such world class infrastructure, students can gain top notch education in a comforting environment. </p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/11.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Green Campus</a></h3>
                                   
                                  <p>Naipunnya believes in learning from nature and sustaining it for posterity. The eco-friendly campus helps in inculcating a love for the color green. As schools go green, their students and employees will learn how to incorporate green ideas into their everyday lives. The trend of going green is becoming a way of life on college campuses and beyond.</p>
                                   
                                </div>
                                
                            </article>
                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/13.png" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Parking facilities</a></h3>
                                   
                                  <p>Parking facilities are provided for students and visitors on the campus as designated on the campus map. Motorcycle parking is permitted only in specially designated areas.  Visitors on campus have to obtain temporary parking permits from security personnel.</p>
                                   
                                </div>
                                
                            </article>

                             <article class="post post-full">
                                <div class="post-image col-sm-5">
                                    <div class="image">
                                        <img src="images/Infrastructure/15.jpg" alt="">
                                        
                                    </div>
                                </div>
                                <div class="post-content col-sm-7">
                                    <h3 class="post-title"><a href="#">Fitness Center</a></h3>
                                   
                                  <p>A Well equipped gymnasium is available within campus for students as well as faculty. Physical activity is good for mental health and experts believe that exercise releases chemicals in the brain  to give a feel good factor. Regular exercise can also boost self-esteem and help concentrate, sleep, look and feel better. </p>
                                   
                                </div>
                                
                            </article>

					 </div>
				</div>
			</div>
			
			<hr class="color-light col-sm-8">
			
		</section>	
			
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>