
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<!-- <h1 class="entry-title">MBA HIGHLIGHTS</h1> -->
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">MBA PROGRAM HIGHLIGHTS</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<section id="content">
			<div class="container">
				<div class="row">
					
					<div id="main">
						
						<div class="section-info" id="nbs-course-features">
                        <h2>Course Features</h2>
                        <div class="box">
                           <h6>Case Study</h6>
					<p>Case Study analysis teaching methodology is utilized at NBS. Case study is a simulation of existed or an existing problem in a company for which different solutions are enumerated, discussed and analyzed by students and shared with other students and finally evaluated by faculty.</p>
					<h6>Debates</h6>
					<p>Debates are initiated in the classroom  on topics in business and current issues to facilitate students to help develop skills in defending at management fests and placements. Debating also helps to build confidence in students  and provides knowledge sharing.</p>
					<h6>Group Discussions</h6>
					<p>Group discussions are initiated in a class rooms on topics of  general interest. Group discussions provides social learning and and helps students to share knowledge, skills, social and ethical values. They also help them to ‘think out of box’ and climb the corporate ladder.</p>
					<h6>Management Games</h6>
					<p>Management Games helps students to innovate business models and facilitates students  active involvement in business practices</p>
					<h6>Outbound Training</h6>
					<p>Outbound training is included in the curriculum in order to enhance overall Personality Development of Students. Outbound training program helps students to undertake field activity and build those skills which cannot be achieved in classroom learning.</p>
					<h6>Rural camp</h6>
					<p> Rural camps are an integral part of practice learning and provides a broad learning framework. Rural camps help students in their professional development and enables to bridge the gaps between academic knowledge base and the complex realities of practice.</p>
					<h6>Presentation</h6>
					 <p>Presentation skills give student to rehearse the knowledge, lessons learnt in the classroom. Students are assessed by individual and Group presentation and give feedback and trained on the gaps to present themselves the best way he / she can be assessed.</p>
					 <h6>Business quiz</h6>
					 <p>Quiz enables students to test students capability to grasp the nuances of the subject and helps them to be </p>
					 <h6>Real-time Projects</h6>
					 <p>Students are sent on real time projects of their own interest. Real time projects help students acquire skills, by involving the live project. This also help students to strengthen the skills they helps in getting right job.</p>
					 <h6>Role Play</h6>
					 <p>Role plays helps student put himself/herself in the shoes of the decision maker. Role Play initiates the student to experience the situation different perspectives and thus helps him/her to gain valuable knowledge based on real life scenarios.</p>
					 <h6>Global Exposure Program (GEP)</h6>
					 <p>NBS believes in creating business leaders for the world community. In order to realize this mission,
we have introduced the Global Exposure Program (GEP). Students of NBS are sent for a Foreign
Tour during the Second Academic Year for a duration of 4-5 days. The countries chosen are mostly
the developing Economies in the Asia. Students are required to conduct a detailed study on the
country being visited and prepare reports on areas like Governance &amp; Administration, Industrial
Environment, Public Transport System &amp; Logistics, opportunities for Trade and Commerce with India
and also Entrepreneurship in that country. Visits are organized to Educational Institutes, Industries,
Public Facilities, Ports, etc with a view to expose students to best practices being implemented.
During the spare time students are taken around to places of tourist interest, malls, general site
seeing, cultural shows etc. Students have visited destinations in East Asian countries like Malaysia
as part of the Global Exposure Program.</p>
                        </div>
                    </div>

                    <div class="section-info" id="nbs-placement">
                    	 <h2>Placement Support</h2>
                    	 <p >Our Placement Centre exists to support you in realising your full career potential, from career planning, to making effective applications and interview preparation. Support for MBA students includes:
                    	 </p>
                    	 <ul class="disc size-medium">
                            <li>Workshops designed to stimulate your career planning</li>
                             <li>Prepare you to compete effectively in the dynamic jobs market</li>
                              <li>Participation in Placement Fairs</li>
                               <li>Placement Brochure </li>
                                <li>Access to professional qualified Placement Consultants</li>
                                 <li>Access to Alumni networks</li>
                                   
                         </ul>
                    </div>

                    <div class="section-info" id="nbs-accreditations">
                    	 <h2>Accreditations & Industry partners</h2>

							<div class="row">
								<div class="col-sms-6 col-sm-6 col-md-2">
									<div class="team-member style-colored box">
										<div class="image-container">
											<img src="images/Accreditations/1.jpg" alt="">
											
										</div>
									</div>
								</div>

								<div class="col-sms-6 col-sm-6 col-md-2">
									<div class="team-member style-colored box">
										<div class="image-container">
											<img src="images/Accreditations/2.jpg" alt="">
											
										</div>
									</div>
								</div>

								<div class="col-sms-6 col-sm-6 col-md-2">
									<div class="team-member style-colored box">
										<div class="image-container">
											<img src="images/Accreditations/3.jpg" alt="">
											
										</div>
									</div>
								</div>

								<div class="col-sms-6 col-sm-6 col-md-2">
									<div class="team-member style-colored box">
										<div class="image-container">
											<img src="images/Accreditations/4.jpg" alt="">
											
										</div>
									</div>
								</div>

								<div class="col-sms-6 col-sm-6 col-md-2">
									<div class="team-member style-colored box">
										<div class="image-container">
											<img src="images/Accreditations/5.jpg" alt="">
											
										</div>
									</div>
								</div>

								<div class="col-sms-6 col-sm-6 col-md-2">
									<div class="team-member style-colored box">
										<div class="image-container">
											<img src="images/Accreditations/6.jpg" alt="">
											
										</div>
									</div>
								</div>


							</div>
                    	 <div class="iso-container iso-col-3 style-grid"></div>
                    </div>
                    <p id="nbs-Industrial"></p>
                    <div class="section-info" >
                    	<h2>Industry Interaction</h2>
                    	
                    	<p>The Industry Interaction acts as the window that connects NBS to the outside corporate world. Industry Interaction provides the platform wherein students, academia, industry stalwarts and achievers from all fields of work come together and share their ideas, vision and culture. Through its various initiatives round the year, it ensures that the budding managers stay in touch with the real world, even while honing their skills at the classrooms. Industry Interactions include guest lectures by various Industry representatives to inculcate the Industrial facets of the curriculum. Industrial visits are also a part of Industry interaction. It help to bridge the gap between classroom and real world. Through industrial visit NBS students are opened to real world, where they get to know what is management and what is their future profession. Industrial visits also provides an opportunity to gather information about academic lessons, which cannot be visualized in classrooms. All students are mandatorily sent for Industrial visits in every semester. The first three will be national and the final will be International subject to students requirements. NBS has established linkages with several industries and institutes to facilitate its students to have better industrial exposure.</p>
                    	
                    </div>

					</div>
				</div>
			</div>
			
		</section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>