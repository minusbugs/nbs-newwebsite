
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Fees Structure</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">Fees Structure</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<section id="content">
			<div class="container">
				<div id="main">
					<h3 class="section-title">Fees</h3>
					<p>MBA program fee for the academic year 2018- 19 will be Indian Rupees 2,91,700/- (nonrefundable)  payable in four instalments as follows:</p>
					<div class="details">
						<ul class="disc size-medium">
							<li>By 23rd July, 2018 (or last date of acceptance) : Rs.72,375/- (1st instalment) </li>
							<li>By 20th December, 2018 (or date of registration) : Rs.71,250/- (2nd instalment) </li>
							<li>By 23rd July, 2019 : Rs.74,825/- (3rd instalment) </li>
							<li>By 20th December, 2019 : Rs.72,250/- (4th instalment) 	</li>
						</ul>
					</div>
					<p>Furthermore an amount of Rs.5,000/- has to be paid as refundable caution deposit at the time of registration. <br/>
					The programme fee includes tuition fees, cost of study material, partial cost of study tours organised by Naipunnya Business School (NBS). However, students will have to bear the expenses for personal travel, uniform and miscellaneous. expenses and any other expenses of personal nature during their course at NBS.</p>
					<h3 class="section-title">Merit cum Means Scholarships</h3>
					<p>The vision of Naipunnya group to "reach the unreachable" thus we at NBS does not want any student be denied opportunity to pursue the MBA program for want of adequate financial resources. Substantial need-based Scholarships are available for meritorious students belonging to economically disadvantaged families. </p>
					<p><span style="font-size: medium;font-weight: 600;">NBS Merit Scholarships:</span>  These are given to select  students based on the admission rank and their performance in internal and university examinations. More information on the scholarships are available with the admission office. </p>
					<p><span style="font-size: medium;font-weight: 600;">Govt Scholarships:</span> Central Sector Interest Subsidy Scheme, 2009 on Model Education Loan Scheme of IBA . 
					Under this Scheme Interest Subsidy is given during the moratorium period i.e., Course period plus one year on Education Loan taken from the Scheduled Banks under the Model Education Loan Scheme of Indian Banks Association to students belonging to economically weaker sections whose annual parental income is up to Rs. 4.5 Lakh from all sources. The subsidy is allowed for undergoing recognised Professional/ Technical courses in recognised Institutions in India. This subsidy is allowed only once. The Nodal Bank is Canara Bank, Bengaluru.</p>
					<h3 class="section-title">Fees Structure for MBA program 2017-18</h3>
					<table class="table table-bordered">
						<tr>
							<th>Head of account</th>
							<th>1 st Semester</th>
							<th>2 nd Semester</th>
							<th>3 rd Semester</th>
							<th>4 th Semester</th>
						</tr>
						<tr>
							<td>Admission</td>
							<td>500</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						</tr>
						<tr>
							<td>Tuition</td>
							<td>60,000</td>
							<td>60,000</td>
							<td>60,000</td>
							<td>60,000</td>
						</tr>
						<tr>
							<td>University fees</td>
							<td>1875</td>
							<td>1250</td>
							<td>1825</td>
							<td>2250</td>
						</tr>
						<tr>
							<td>Student
development fee</td>
							<td>10,000</td>
							<td>10,000</td>
							<td>10,000</td>
							<td>10,000</td>
						</tr>
						<tr>
							<td></td>
							<td>72,375</td>
							<td>71,250</td>
							<td>74,825</td>
							<td>72,250</td>
						</tr>
					</table>
					

					

					
				</div>
			</div>
		</section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>