
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Student Project</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">Students Project</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<section id="content">
            <div class="container">
                <div class="row">
                    
                    <div id="main" class="col-sm-12">
                       <h3>Major Project</h3>
					  <p>The students shall do a major project during their final semester of MBA under a faculty guide, preferably in their area of specialization. The duration of fieldwork for major project is eight weeks. This project work is to be done individually by the students. The student shall prepare and submit a project report, printed and bound (preferably spiral bound) with a minimum of 100 A4 pages of text, to the Head of the Department or Centre or Institute before the last working day of the final semester. A certificate showing the duration of the project work shall be obtained from the organization for which the project work was done and it shall be included in the project report.</p>
					   <p>The head of the institute shall send the projects of all the students together to the Controller of Examinations well in time so that they are received in the Pareeksha Bhavan within two weeks from the last date for project submission to the institute. Projects received late shall be forwarded to the Controller of Examinations along with a request for late submission supported by necessary fee for late submission as fixed by the University. However, such late submission shall be done within one month of the last date for final semester project submission.
</p>
					   <h3> Organization study</h3>
						
						<ul class="star size-medium">
					   <li>The Students are sent to various industries /organization to have an exposure to the real world of business.</li>
					   <li>It is a time to have a hands on experience on how functional management concept are practiced in the organization.</li>
					   <li>Having studied about the various departments of an organization in the classrooms this study should help you to explore and find how they function in the real organization atmosphere
</li>
					   </ul>
					  
                        </div>
                    </div>
                </div>
            </div>
        </section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>