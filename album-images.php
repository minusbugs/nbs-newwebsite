
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
            <div class="page-title">
                <div class="container">
                    <h1 class="entry-title">Gallery</h1>
                </div>
            </div>
            <ul class="breadcrumbs">
                <li><a href="index.html">Home</a></li>
                
                <li class="active">Gallery</li>
            </ul>
        </div>
			
		</div>
		<!-- HEADER -->
	
		
			<!-- CONTENT-Features -->
			<div class="container-fluid">
				<div id="main">
					

					<div class="section-info">
                        
                        <div class="soap-gallery metro-style gallery-col-3">
                            <div class="gallery-wrapper">
							
							<?php 
							include("db/dbConn.php");
							$id=$_GET["id"];
							$sql = "SELECT * FROM gallery where GalleryId=".$id;
							$result = $conn->query($sql);
							if ($result->num_rows > 0) {
							while($row = $result->fetch_assoc()) { 
								$path=$row["ImagePath"];
							?>

                               
                                <a href="http://admin.nbs.ac.in/assets/Gallery/<?php echo $path; ?>" class="image hover-style3"><img src="
                                    
									<?php 						
									echo 'http://admin.nbs.ac.in/assets/Gallery/'.$path.'" alt="">'; 
									?>
                                    <div class="image-extras" style="display: none; left: 0px; top: -100%; transition: all 300ms ease;"></div>
                                </a>
								<?php }
							} else {
							echo "0 results";
							}
							$conn->close();
							?>
                                
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			
			<section id="content"></section>
		
			<!--FOOTER-->
			
			<?php include("inc/footer.php") ?>
			
			<!-- Javascript -->
			<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
			<script type="text/javascript" src="js/jquery.noconflict.js"></script>
			<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
			<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
			<!-- Twitter Bootstrap -->
			<script type="text/javascript" src="js/bootstrap.min.js"></script>
			<!-- Magnific Popup core JS file -->
			<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			<!-- parallax -->
			<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
			
			<!-- waypoint -->
			<script type="text/javascript" src="js/waypoints.min.js"></script>
			<!-- Owl Carousel -->
			<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
			<!-- load revolution slider scripts -->
			<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
			<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
			<!-- plugins -->
			<script type="text/javascript" src="js/jquery.plugins.js"></script>
			<!-- load page Javascript -->
			<script type="text/javascript" src="js/main.js"></script>
			<script type="text/javascript" src="js/revolution-slider.js"></script>
			
			
		</body>
	</html>