
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Faculty</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">Faculty</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<div class="single-post">
			<section id="content">
				<div class="container">
					<div id="main">                    
					<?php 
					
include("db/dbConn.php");
$sql = "SELECT * FROM faculty";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) { ?>
						<article class="post box-lg">
							
							<div class="about-author box">
								<div class="author-img">
								<?php $dir="http://admin.nbs.ac.in/assets/Faculty/";
								      $img=$row["FacultyImage"];
                                      $images = $dir.$img;
												
									echo '<span><img src="'.$images.'" alt="'. $row["FacultyName"].'" class="facimg"></span>';?>
								</div>
								<div class="about-author-content">
									<div class="social-icons">
										<a href="#" class="social-icon"><i class="fa fa-twitter has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-facebook has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-google-plus has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-skype has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-tumblr has-circle"></i></a>
									</div>
									
									<span class="nbs-author-name"><?php echo $row["FacultyName"];?></span>									
									<p><?php echo $row["AboutFaculty"]; ?></p>
								</div>
							</div>
							
						</article>
<?php }
} else {
    echo "0 results";
}
$conn->close();
?>
						
					</div>
				</div>
			</section>
		</div>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>