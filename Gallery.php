
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
            <div class="page-title">
                <div class="container">
                    <h1 class="entry-title">Gallery</h1>
                </div>
            </div>
            <ul class="breadcrumbs">
                <li><a href="index.html">Home</a></li>
                
                <li class="active">Gallery</li>
            </ul>
        </div>
			
		</div>
		<!-- HEADER -->
	
		 <section id="content">
            <div class="container">
                <div id="main">
                    <div class="blog-posts row">

            		<?php 
						include("db/dbConn.php");
					$sql = "SELECT * FROM gallery where GalleryId=0";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
					// output data of each row
					while($row = $result->fetch_assoc()) { 
						$path=$row["ImagePath"];
					?>
					<div class="col-sm-6 col-md-4">
                            <article class="post post-grid">
                                <div class="post-image">
                                    <div class="image-container">
                                        <a href="album-images.php?id=<?php echo $row["ImageId"]; ?>"><img alt="" src="http://admin.nbs.ac.in/assets/Gallery/<?php echo $path; ?>"></a>
                                    </div>
                                </div>
                                <div class="post-content">
                                   <!-- <div class="post-date"><span>12 december 2014</span></div> -->
                                    <h4 class="entry-title gallery-name"><a href="album-images.php?id=<?php echo $row["ImageId"]; ?>"><?php echo $row["ImageName"]; ?></a></h4>
                                     <div class="post-meta">
                                     
                                    </div>
                                    
                                    
                                </div>
                            </article>
                        </div>

                        <?php
                    }
	                    }
	                    ?>
                    </div>
                </div>

            </div>
        </section>



		
			<!--FOOTER-->
			
			<?php include("inc/footer.php") ?>
			
			<!-- Javascript -->
			<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
			<script type="text/javascript" src="js/jquery.noconflict.js"></script>
			<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
			<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
			<!-- Twitter Bootstrap -->
			<script type="text/javascript" src="js/bootstrap.min.js"></script>
			<!-- Magnific Popup core JS file -->
			<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
			
			<!-- parallax -->
			<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
			
			<!-- waypoint -->
			<script type="text/javascript" src="js/waypoints.min.js"></script>
			<!-- Owl Carousel -->
			<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
			<!-- load revolution slider scripts -->
			<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
			<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
			<!-- plugins -->
			<script type="text/javascript" src="js/jquery.plugins.js"></script>
			<!-- load page Javascript -->
			<script type="text/javascript" src="js/main.js"></script>
			<script type="text/javascript" src="js/revolution-slider.js"></script>
			
			
		</body>
	</html>