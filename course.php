<?php include("inc/side-menu.php"); ?>
<!-- BANNER -SLIDER -->
<div class="page-title-container row">
	<div class="page-title">
		<div class="container">
			<h1 class="entry-title">Course : MBA</h1>
		</div>
	</div>
	<ul class="breadcrumbs">
		<li><a href="index.php">Home</a></li>
		<li class="active">Course : MBA</li>
	</ul>
</div>

</div>
<!-- HEADER -->


<!-- CONTENT-Features -->
<section id="content">
<div class="container">
	<div class="row">
		
		<div id="main" class="col-sm-12">
			<h3>Highlights of the MBA program</h3>
			<ul class="star size-medium">
				<li>Each student of NBS will be skilled in understanding the implications of integrated business processes in managing an enterprise.</li>
				<li>Students will analyze ineffective and unethical business practices from live cases in the business world.</li>
				<li>Each student will be ethical, socially responsible and will demonstrate the triple bottom of sustainability, in financial, social and environmental when making business   decisions.</li>
				<li>Students will offer solutions which are useful for the community and the business environment.</li>
				<li>Students will evaluate business decisions with regard to their impacts on environmental sustainability.</li>
				<li>Students will develop the capacity to synthesize and analyse information as to make sound business decisions.</li>
				<li>Students will apply a systematic approach to solving business problems.</li>
				<li>Students will evaluate financial statements and documents to support business decisions.</li>
				<li>Students will use appropriate technologies in gathering and analyzing data relevant to managerial decision-making.</li>
				<li>Each student will be a leader and/or manager who understands group dynamics and is capable of influencing others to achieve organizational goals.</li>
			</ul>
			<br>
			<h3>Key Strengths of the MBA program</h3>
			<ul class="star size-medium">
				<li>Best in class Infrastructure & Well equipped Computer Labs</li>
				<li>Airy Class rooms with Internet Connectivity, LCD</li>
				<li>Industry experienced, Academically competent & committed Faculty</li>
				<li>3 Faculty Members with PhD & 4 on final stages of PhD</li>
				<li>Student-centered Teaching</li>
				<li>Immersive & Experiential Learning Process</li>
				<li>Consistent placements in National & Multinational Companies</li>
				<li>Regular winners in National Business Plan & Management fests</li>
				<li>Committed Holistic development of students</li>
				<li>Environmental friendly campus</li>
				<li>Regular Industry-Institute Interaction through Industry visit & Guest Lecture from Industry</li>
				<li>Practices the triple bottom line in education through addressing social, environmental & economic issues </li>
			</ul>
			<h3>Global Exposure Program (GEP)</h3>
			<p>
				NBS believes in creating business leaders for the world community. In order to realize this mission,
we have introduced the Global Exposure Program (GEP). Students of NBS are sent for a Foreign
Tour during the Second Academic Year for a duration of 4-5 days. The countries chosen are mostly
the developing Economies in the Asia. Students are required to conduct a detailed study on the
country being visited and prepare reports on areas like Governance &amp; Administration, Industrial
Environment, Public Transport System &amp; Logistics, opportunities for Trade and Commerce with India
and also Entrepreneurship in that country. Visits are organized to Educational Institutes, Industries,
Public Facilities, Ports, etc with a view to expose students to best practices being implemented.
During the spare time students are taken around to places of tourist interest, malls, general site
seeing, cultural shows etc. Students have visited destinations in East Asian countries like Malaysia
as part of the Global Exposure Program.</p>
			<!-- <ul class="star size-medium">
				<li>Respect for the rights, differences, and  have a cultural immersion the larger community</li>
				<li>Practice honesty, transparency in all their dealing with members of the community</li>
				<li>To be a person who is prepared to change behavior, accept norms and be part of the community</li>
			</ul>
			<br>
			<p>NBS strives to be a living model of these values. To this end, NBS community members have a personal responsibility to integrate these values into every aspect of their experience here. Through our personal commitment to these values, we can change the economic and social for the good of all.</p>
			 -->
			
			
		</div>
	</div>
</div>
</div>
</section>


<!--FOOTER-->
<?php include("inc/footer.php") ?>
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery.noconflict.js"></script>
<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Magnific Popup core JS file -->
<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- parallax -->
<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
<!-- waypoint -->
<script type="text/javascript" src="js/waypoints.min.js"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
<!-- load revolution slider scripts -->
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- plugins -->
<script type="text/javascript" src="js/jquery.plugins.js"></script>
<!-- load page Javascript -->
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/revolution-slider.js"></script>
</body>
</html>