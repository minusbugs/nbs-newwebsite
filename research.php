<?php include("inc/side-menu.php"); ?>
<!-- BANNER -SLIDER -->
<div class="page-title-container row">
	<div class="page-title">
		<div class="container">
			<h1 class="entry-title">Research</h1>
		</div>
	</div>
	<ul class="breadcrumbs">
		<li><a href="index.php">Home</a></li>
		
		<li class="active">Reasearch Paper And Publication</li>
	</ul>
</div>

</div>
<!-- HEADER -->
<!-- CONTENT-Features -->
<section id="content">
<div class="container">
	<div class="row">
		
		<div id="main" class="col-sm-12">
			<div class="section-info">
				<p>NBS encourages faculty members and students to publish their research work in reputed National/ International Journals. Since 2015, every year publications are required from faculty members as per UGC norms. The college provides Registration Fee and TA/DA for attending National & International conferences. Faculty are also requested to publish papers in the in-house conference proceedings “Vaidakthya”.  Faculty publications of NBS are given below: </p>
				<div class="row">
					<div class="col-md-12 box">
						<div class="panel-group" id="accordion-5">
							
							<div class="panel style5">
								<h5 class="panel-title">
								<a href="#acc5-1" data-toggle="collapse" data-parent="#accordion-5" class="active">
									<span class="open-sub"></span>
									Rev. Dr. Paulachan K Joseph
								</a>
								</h5>
								<div class="panel-collapse collapse in" id="acc5-1">
									<div class="panel-content">
										<p>P. K. Joseph, M. Rai and G. Joseph, "A Study on the Impact of Self Help Groups (SHGs) on the Financial, Emotional and Socio Political Empowerment of Women in Kerala: A Case Study of Welfare Services, Ernakulam.," in 6th International Research Conference on Entrepreneurship, Mumbai, 2012.</p>
									</div>
									<div class="panel-content">
										<p>P. K. Joseph, L. Nandanwar and M. Mauzib, "A Study to know the Level of Awareness of Entrepreneurship and Entrepreneurial environment in B-Schools and its Impact on Student Community: A Study among Management Students of Navi Mumbai.," in 6th International Research Conference on Entrepreneurship, Mumbai, 2012.</p>
									</div>
									<div class="panel-content">
										<p>P. K. Joseph, L. Nandanwar and H. Joseph, "A Study to know the Level of Awareness of Entrepreneurship and Entrepreneurial environment in B-Schools and its Impact on Student Community: A Study among Management Students of Kerala.," in 6th International Research Conference on Entrepreneurship, Mumbai, 2012.</p>
									</div><div class="panel-content">
									<p>P. K. Joseph, A. Shrivastava and C. Paul, "A Study on the Impact of the Involvement of Local Residents on Tourism: A Case Study of GAVI Village, Kerala," in Concepts & Practices of Retail Management: A Case Based South East Asian Perspective., Mumbai, 2012.</p>
								</div><div class="panel-content">
								<p>P. K. Joseph, V. Chavan and P. P. Prince, "The Impact of Change in 'Shopping Orientation' on the Lifestyle and Behaviour of Consumers with reference to Navi Mumbai.," in Concepts & Practices of Retail Management: A Case Based South East Asian Perspective., Mumbai, 2012.</p>
							</div>
						</div>
					</div>
					<div class="panel style5">
						<h5 class="panel-title">
						<a href="#acc5-2" data-toggle="collapse" data-parent="#accordion-5" class="active">
							<span class="open-sub"></span>
							Dr. Jacob P. M., HoD
						</a>
						</h5>
						<div class="panel-collapse collapse" id="acc5-2">
							<div class="panel-content">
								<p>Mathew Jacob (2016) A study on Attrition among Insurance Agents in Chennai city Paper presented at Vaidakthya, “Trends and Challenges in Enterprise Management” on Naipunnya Business School, 20th May, 2016 (pp 1-6).</p>
							</div>
							<div class="panel-content">
								<p>Feby. A. & Mathew Jacob (2016) “Brand Perception towards Mother’s Rice in Ernakulam District” Paper presented at Vaidakthya, “Trends and Challenges in Enterprise Management” on Naipunnya Business School, 20th May, 2016 (pp 1-6). </p>
							</div>
							<div class="panel-content">
								<p>A. R. S. Pillai, & Mathew Jacob (2016) “A Study on Customer Satisfaction towards Back Water Tourism in Alleppey” Paper presented at Vaidakthya, “Trends and Challenges in Enterprise Management” on Naipunnya Business School, 20th May, 2016 (pp 7-11).</p>
							</div>
							<div class="panel-content">
								<p>Dhaya A. &  Mathew Jacob (2016) “A Study on Emotional Intelligence and its Effect on Employee Performance at KLF Nirmal Industries Private Ltd. Paper presented at Vaidakthya, “Trends and Challenges in Enterprise Management” on Naipunnya Business School, 20th May, 2016 (pp 28-33). </p>
							</div>
							<div class="panel-content">
								<p>J. Davis & Mathew Jacob (2016) “Stress and its Impact on Job Performance of Nurses at LF Hospital & Research Centre, Vaidakthya “Trends and Challenges in Enterprise Management” on Naipunnya Business School, 20th May, 2016 (pp 122-127).</p>
							</div>
							<div class="panel-content">
								<p>Jithin Benedict, Sini P. K, & Mathew Jacob (2016), “A Study on the Perception of Online Shoppers towards Luxury Products” “Trends and Challenges in Enterprise Management” on Naipunnya Business School, 20th May, 2016 (pp 83-90). Mathew Jacob (2010) “Internet Addiction and relationships formed on Internet among University students” Karunya Journal of Research, Volume 2, Issue 1, January 2010. ISSN-0974-7214.</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Benedict Jithin. (2015), Motorola case study Erudition, The Albertian Journal of Management Vol.10 (1), 25-34. ISSN: 0973-7839.</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob & Benedict Jithin (2015) “An exploratory study on the Prevalence of Road Rage in Cochin City. International Journal of Commerce and Management Research (PIJCMR), Vol.1 (1), 105-110.ISSN:2321-3604. </p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2014), “Good Governance through Social networking” published by ICBO IFEM-2014, CHENNAI</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2014), “Advertising through Social Networks- Prospects and Problems” published by Adaikalamatha Institute of Management, Thanjavur ISSN 2250-1940</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2014), “Social networking among HR professionals” published by Sree Narayana Gurukulam college of Engineering, Ernakulam ISBN 978-93-82338-84-0</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013),  “The use of Social networking sites for Disaster Management” published by Noorul Islam University ISSN No 0973-3957</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013), “Homeschooling through Social networking sites” published by Pioneer institute of Professional studies, Indore ISSN-0974-8954-SHODH</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013),  “The role of Social networking among HR Professionals” published by R B institute of Management studies, Ahmedabad ISSN No.2321-869</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013), “The latest trend in online marketing Advertising through Social Networks- Prospects and Problems” published by Sinhgad College of Engineering, Department of Management studies  ISBN 978-93-80258-20-1 </p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013), “Social networking - a boon for tourism” published by Prestige Institute of Management, Gwalior ISBN:978-93-82951-49-0</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013), “Job satisfaction and online Social networking” published by Department of Business Administration, Annamalai University, Chidambaram, Tamilnadu</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013) “Social networking sites a boon to the entrepreneur published by The Gandhigram Rural institute Deemed university, Dindigul, Tamilnadu ISSN: 2231-4911</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan, and Srivastava Sheela (2013), “Social networking sites for HR recruitment” published by Carmel college, Pathanamthitta, Kerala ISSN 0968-4690), Vol.1 (2), January, 2014</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013), “The effect of social networking in office on Job satisfaction and Job performance,  Holy Grace Academy of Management Studies, Mala, Thrissur, Kerala ISSN:0975-3427</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013) “The role of social networking sites for pre-employment screening, Holy Grace Academy of Management studies, Mala, Thrissur, Kerala ISSN:0975-3427</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013) “ Role of social networking in financial investment services” presented at the National Seminar on ‘Innovations and strategic business practices in India’, on October 15th and 16th, 2014 ISBN 978-93-82338-27-7.</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob and Anandanatarajan (2012) “Job satisfaction and Online Social networking” published in Theoretical Perspectives of Management Policies, Bonfring, 2012. ISBN 978-93-82338-13-0.</p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2012) “A study on online Social Networking & relationships among University students in Thailand” published by Holy Grace Management Review ISSN No. 0975-3427.</p>
							</div>
							<div class="panel-content">
								<p>Srivastava Sheela and Mathew Jacob (2012) A comparative study on product differentiation and diverse marketing strategies adopted by ICICI, HDFC and AXIS Bank, Faculty of Management studies, BHU, ISBN 978-93-820-62-73-8. </p>
							</div>
							<div class="panel-content">
								<p>Mathew Jacob, Anandanatarajan and Srivastava Sheela (2013), “A study examining the role of Spiritual leadership in Organizations” Velammal College of Engineering and Technology, Madurai, TN ISBN:978-81-925487-9-1</p>
							</div>
							
						</div>
					</div>
					<div class="panel style5">
						<h5 class="panel-title">
						<a href="#acc5-7" data-toggle="collapse" data-parent="#accordion-5" class="active">
							<span class="open-sub"></span>
							Prof. Sabu Varghese
						</a>
						</h5>
						<div class="panel-collapse collapse" id="acc5-7">
							<div class="panel-content">
								<p>Varghese, S., & G, Hari Sundar. (2017). The effect of Psychological Contract Breach on Turnover Intention: A conceptual study with Ethical Climate as the moderator. Vaidakthya'17 (pp. 57-59). Koratty: Naipunnya Institute of Management and Information Technology.</p>
							</div>
							<div class="panel-content">
								<p>Sunny T, R., Raj D., P., & Varghese, S. (2016, September). Strategic study on quality of work life among railway loco workers - a study with reference to loco employees of Ernakulam, Kerala. Intercontinental Journal of Human Resource Research Review, 4(4).</p>
							</div>
							<div class="panel-content">
								<p>Varghese, S., & G, Hari Sundar. (2016, May). An analysis of Corporate Social Responsibility expenditure of Indian companies in the light of new CSR legislation. Vaidakthya '16 (pp. 77-82). Koratty: Naipunnya Institute of Management and Information Technology.</p>
							</div>
							<div class="panel-content">
								<p>Varghese, S., & G, Hari Sundar. (2016, July). Reassessing the effect of Psychological Contract Breach on corruption with ethical adherence and behavioral integrity as moderators: A conceptual study. International Journal of Commerce and Management Research, 171-176.</p>
							</div>
							<div class="panel-content">
								<p>Sabu. V. Nijo. V. (2015), Antecedents of employee engagement and its effect on Employee satisfaction, Erudition, The Albertian Journal of Management Vol.9 (2), 25-34. ISSN: 0973-7839.</p>
							</div>
							
						</div>
					</div>
					<div class="panel style5">
						<h5 class="panel-title">
						<a href="#acc5-3" data-toggle="collapse" data-parent="#accordion-5" class="active">
							<span class="open-sub"></span>
							Prof. Nijo Varghese
						</a>
						</h5>
						<div class="panel-collapse collapse" id="acc5-3">
							<div class="panel-content">
								<p>Varghese, N., & G, H. S. (2017). Influence of Online Visual Merchandising Cues on Purchase Intention. Vaidakthya’17 - Recent Trends in E-Business: Opportunities and Challenges (pp. 60-62). Pongam:Naipunnya Business School.</p>
							</div>
							<div class="panel-content">
								<p>Varghese, N., & G, H. S. (2017). Impact of E-satisfaction on e-WOM Intention: Moderating Effect of Desire for Online Social Interaction. Emerging Paradigms in Business: Marketing and HR Perspectives (pp. 75-85). Kochi: CUSAT.</p>
							</div>
							<div class="panel-content">
								<p>Varghese, N. (2016, November). Influence of Store Ambience and Merchandising on Customer Loyalty in Food Retailing. Gurukulam Journal of Management Research, 4(1), 18-24.</p>
							</div>
							<div class="panel-content">
								<p>Varghese, N. (2016, May) Elements of E-tailing Trust and its Effect on Online Purchase Intention Vaidakthya'16 - National Conference on Trends and Challenges in Enterprise Management (Proceedings), ISBN 978-93-5265-481-9, 12-17.</p>
							</div>
							<div class="panel-content">
								<p>Varghese, N. (2016, May) Role of Packaging on Impulse Buying of Packaged Food Products. Vaidakthya'16 - National Conference on Trends and Challenges in Enterprise Management (Proceedings), ISBN 978-93-5265-481-9, 72-76.  </p>
							</div>
							<div class="panel-content">
								<p>Varghese, N. (2015, July). Antecedents of Employee Engagement and its Effect on Employee Satisfaction. Erudition - The Albertian Journal of Management ISSN 0973-7839, Volume 9, Issue 2, 25-34.</p>
							</div>
						</div>
					</div>
					<div class="panel style5">
						<h5 class="panel-title">
						<a href="#acc5-4" data-toggle="collapse" data-parent="#accordion-5" class="active">
							<span class="open-sub"></span>
							Prof. Jithin Benedict
						</a>
						</h5>
						<div class="panel-collapse collapse" id="acc5-4">
							<div class="panel-content">
								<p>Mathew, J & Benedict, Jithin (2017). A study on customer satisfaction towards online taxi services. Vaidakthya'17 - Recent Trends in E- Business: Opportunities and challenges, 15.</p>
							</div>
							<div class="panel-content">
								<p>Benedict, Jithin (2017) & Mathew, J. A study on the perception of online shoppers towards luxury products. . Vaidakthya'16 - National Conference on Trends and Challenges in Enterprise Management (Proceedings), 83.</p>
							</div>
							<div class="panel-content">
								<p>Benedict, Jithin & Joseph, K (2017). An exploratory study on retailer relationship management in Dairy Industry; with special reference to PDDP. Vaidakthya'16 - National Conference on Trends and Challenges in Enterprise Management, 58.</p>
							</div>
							<div class="panel-content">
								<p>Benedict, Jithin. &. G, Hari Sundar (2017). Comparison of online and instore shopping preference of luxury goods, A conceptual Study on Indian youth. . Vaidakthya'16 - Recent Trends in E- Business: Opportunities and challenges.</p>
							</div>
							<div class="panel-content">
								<p>Benedict, J., Sini P., & Mathew Jacob, “A Study on the Perception of Online Shoppers towards Luxury Products” “Trends and Challenges in Enterprise Management” on Naipunnya Business School, 20th May, 2016 (pp 83-90).
								</p>
							</div>
							<div class="panel-content">
								<p>G, Hari Sundar & Benedict, Jithin (2016). Conceptual study on service quality and relationship quality research. International Organization of Scientific and management Research, 7-12.
								</p>
							</div>
							<div class="panel-content">
								<p>G, Hari Sundar & Benedict, Jithin (2016). Study on Music and Amiability of Sales Personnel on Consumer Behaviour Strategic study with reference to Retail Setting and Emphasis on Gender Difference. Asian Journal of Research in Marketing, 44-48.
								</p>
							</div>
							<div class="panel-content">
								<p>Mathew, J & Benedict, Jithin (2016). A Study on consumer’s attitude towards Mobile Advertisements. Erudition - The Albertian Journal of Management, 2016, 7-13.
								</p>
							</div>
							<div class="panel-content">
								<p>Mathew, J & Benedict, Jithin (2016). An exploratory study on the prevalence of road rage in Cochin city. Primax International Journal of Commerce and Management Research, 105-110.
								</p>
							</div>
							<div class="panel-content">
								<p>Mathew, J & Benedict, Jithin (2015). Rebranding through social media – A case study of Motorola mobile phones. Erudition - The Albertian Journal of Management, 2016.
								</p>
							</div>
						</div>
					</div>
					<div class="panel style5">
						<h5 class="panel-title">
						<a href="#acc5-8" data-toggle="collapse" data-parent="#accordion-5" class="active">
							<span class="open-sub"></span>
							Prof. Bhuvanes Kumar
						</a>
						</h5>
						<div class="panel-collapse collapse" id="acc5-8">
							<div class="panel-content">
								<p>Bhuvanes, K. (2016),  Financial Inclusion and its progress in India : A comparison with China, Vaidakthya'16 - National Conference on Trends and Challenges in Enterprise Management (Proceedings)  ISBN 978-93-52654-81-9 Pages 33-35.</p>
							</div>
							<div class="panel-content">
								<p>Bhuvanes, K. (2017), Applications of Big data analysis in Indian retail industry, Vaidakthya'17 - National Conference on Recent Trends in E-business: Opportunities and Challenges (Proceedings) ISBN 978-93-5268-538-7 Pages 5-10.</p>
							</div>
							
							
						</div>
					</div>
					<div class="panel style5">
						<h5 class="panel-title">
						<a href="#acc5-5" data-toggle="collapse" data-parent="#accordion-5" class="active">
							<span class="open-sub"></span>
							Prof. (Mrs.) Asha Antony
						</a>
						</h5>
						<div class="panel-collapse collapse" id="acc5-5">
							<div class="panel-content">
								<p>Micro finance for super power 2020: National level conference on contemporary practices for the success of indian business [Rvs faculty of management] 04/02/2011 978-81-909150-4-5 [PG.202]
								</p>
							</div>
							<div class="panel-content">
								<p>Role of microfinance in economic development of india International seminar on global challenges of emergent India- a management perspective [Vivekanandha institute of information & management studies] 14/02/2011 978-81-9104-720-2 [PG.14]</p>
							</div>
							<div class="panel-content">
								<p>“Micro Finance and Financial Inclusion” National work shop on inclusive growth in India [The Gandhigram Rural Institute - deemed university] 22/02/2011 81-7888-615-4
								</p>
							</div>
							<div class="panel-content">
								<p>“Excellent Practices among Banks for Inclusive Growth– Empirical Evidences from Recent Literature Survey” International journal of research in commerce and management 15/06/2011 ISSN 0976-2183 VOLUME NO:2[2011], ISSUE NO.6[JUNE] PG NO.91 TO 94</p>
							</div>
							<div class="panel-content">
								<p>Ansoff's Strategic Paradigm For Mfis Sustainability In Inclusive Growth International journal of physical and social science 01/12/2011 ISSN 2249-5894 VOLUME NO:1[2011], ISSUE NO.4[dec] PG NO.299 TO 236
								</p>
							</div>
							<div class="panel-content">
								<p>leveraging MF is it a convergence and or divergence National conference on emerging new trends in managerial excellence 14/10/2011 978-81-909042-4-7 pg no :4-77
								</p>
							</div>
							<div class="panel-content">
								<p>Efficient Positioning For Sustained Leadership Paripex-Indian Journal of Research April,2012 ISSN 2250-1991 VOLUME NO:1[2012], ISSUE NO.4[April] PG185-188
								</p>
							</div>
							<div class="panel-content">
								<p>Impact Of Corporate Governance On The Performance Of Companies In The Stock Market
									International journal of marketing and technology.
									June,2012
									ISSN 2249-1058 VOLUME NO:2[2012], ISSUE NO.[June] PG NO.286 TO 300
								</p>
							</div>
							<div class="panel-content">
								<p>Issues in management education in India
									International journal-research journal of social science and management
									Dec, 2012
									ISSN 2251-1571 VOLUME NO:02, NO.8 DEC 2012
								</p>
							</div>
							<div class="panel-content">
								<p>Workshop on Research methodology
									Mother Teresa women’s university Kodaikanal
									27/08/13
								</p>
							</div>
							<div class="panel-content">
								<p>Entrepreneurship awareness camp
									Anna university
									08/02/14
								</p>
							</div>
							<div class="panel-content">
								<p>Workshop on Census dissemination
									The Gandhigram institute of rural health and family welfare trust
									29/01/15
								</p>
							</div>
							<div class="panel-content">
								<p>Exploring social disorganization in microfinance –SHGs
									Fatima college- national conference on Contemporary issues in society: Socio-Literary perspective
									18/09/15
									ISSN 2349-8684 VOLUME NO.2, SPECIAL ISSUE7 OCT 2015 PG NO.12 TO 16
								</p>
							</div>
							<div class="panel-content">
								<p>Microfinance- A multiple growth pole of Rural India
									The Gandhigram rural institute – deemed University- national seminar on current scenario in business management and technology : A growth pole for promoting rural enterprises
									09/12/15
									Publication under process
								</p>
							</div>
							<div class="panel-content">
								<p>Leveraged microfinance parallel social inclusion
									The Gandhigram rural institute – deemed University- UGC sponsored national seminar on “ social exclusion of people working in unorganized sector in India
									11 & 12 Feb 2016
									Publication under process
								</p>
							</div>
							<div class="panel-content">
								<p>Leveraging Microfinance through managerial skill of SHGs
									Annamalai University –UGC-SAP-DRS-1 Sponsored national seminar on “managerial skill development of SHGs
									29 & 30thMarch 2016
									Publication under process
								</p>
							</div>
							<div class="panel-content">
								<p>Role of Microfinance in Global social leap
									National Centre for rural development, NCRD’s Sterling institute of management studies-international conference –Business 2020:  Issues & Challenges
									16th April 2016
									Published book ISBN : 978-93-5254-816-3
								</p>
							</div>
							<div class="panel-content">
								<p>Conceptual paper on leveraging Microfinance
									International journal of research in Economics and Social sciences
									March 2016
									ISSN 2249-7382, Vol 6, Issue 3 (March 2016)
								</p>
							</div>
							<div class="panel-content">
								<p>Literature Scans Identifies Dearth of Research on Leveraging Microfinance
									AE International Journal of science and technology
									April 2016
									ISSN - 2348 – 6732, Vol 4 - Issue 4 AEIJST - April 2016 -
								</p>
							</div>
							
						</div>
					</div>
					<div class="panel style5">
						<h5 class="panel-title">
						<a href="#acc5-6" data-toggle="collapse" data-parent="#accordion-5" class="active">
							<span class="open-sub"></span>
							Prof. (Dr.) (Mrs.) Ayana Johny
						</a>
						</h5>
						<div class="panel-collapse collapse" id="acc5-6">
							<div class="panel-content">
								<p>Ayana J., (2017), Study on Success and Risk Factors for Technology Adoption with reference to Indian Banking Industry, Conference proceedings Vaidakthya'17 - Recent Trends in E- Business: Opportunities and challenges, Edited book 978-93-5268-538-7.</p>
							</div>
							<div class="panel-content">
								<p>Ayana J., (2015), "An Integrated Model of factors Affecting Information technology Implementation Success in Organizations" International Journal of Engineering and Management Research, "(0nline): 2250-0758, (Print): 2394-6962."
								</p>
							</div>
							<div class="panel-content">
								<p>Ayana J., (2011), "Building Information technology Infrastructure Flexibility for Business Value Generation with reference to the Banking Industry” "conference proceedings of Eleventh Global conference on Flexible Systems Management , IIMK, Kozhikode"  ISBN 978-81-906294-8-5
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>
<!--FOOTER-->
<?php include("inc/footer.php") ?>
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery.noconflict.js"></script>
<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Magnific Popup core JS file -->
<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- parallax -->
<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
<!-- waypoint -->
<script type="text/javascript" src="js/waypoints.min.js"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
<!-- load revolution slider scripts -->
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- plugins -->
<script type="text/javascript" src="js/jquery.plugins.js"></script>
<!-- load page Javascript -->
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/revolution-slider.js"></script>
</body>
</html>