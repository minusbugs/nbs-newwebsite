
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Admissions</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">Admission</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<section id="content">
            <div class="container">
                <div class="row">
                    
                    <div id="main" class="col-sm-12">
                       <h3>MBA admissions</h3>
					   
					   		<p>Any student who has passed any degree of the University of Calicut (including degree programmes of SDE/ Open degree programme of SDE, University of Calicut) or that of any other University or institute or institution recognized by the UGC or AICTE and Calicut university.  Programmes of other Universities or institutions shall be in 10+ 2+ 3 pattern (or 10+ 2+4) under regular stream. Equivalency certificate need to produced from Calicut University for admissions.In all the cases, the student should have passed the bachelor degree examination with not less than 50% marks in aggregate including the marks of languages if any (without approximation, that is, 49.99% is not eligible since it is less than 50%), is eligible for admission. However, SC/ST, OBC, and other eligible communities shall be given relaxation as per University rules.</p> 
					   		<p>The intake for the year 2017-18 is 60 seats.</p>
					  
						<div class="box">
							<a href="Attachments/e-application.pdf" class="btn btn-sm style2">Download Application</a> 
							<a href="enquiry.php" class="btn btn-sm style2">Enquiry</a>
						</div>
					
						<h3>Entrance Examinations</h3>
						<p>The admission to the programme shall be made on the basis of the score in the entrance test called CMAT conducted by the All India Council for Technical Education (AICTE) or KMAT conducted by Admission Supervisory Committee, Govt of Kerala or CAT conducted by consortium of IIMs.</p>
						<p>The minimum qualifying cut off marks of KMAT Kerala Entrance Examination shall be 15% (108/720) for General Category from the academic year 2018. The minimum cut off for SEBC will be 10% (72/720) and 7.5% (54/720) for SC/ST. CMAT & CAT also will have the same criteria. </p>
						<h3>Rank list</h3>
						<p>A rank list shall be prepared by the University based on the final rank score computed as above and a counseling session shall be conducted for admission to the MBA Programme. While preparing the rank list, if there is same index mark for more than one candidate, they will be ranked on the basis of the score of the test. </p>
						<p>The candidates admitted to the MBA programme must produce the qualifying degree mark list/ provisional certificate/ confidential mark list, latest at the last date of closing MBA Admission by the University. If he/she fails to produce the same, his or her admission will be cancelled on the next working day. The University will not be liable for the loss caused to the student. Reservation of seats shall be followed strictly for admission both in the Department and also at its centres as per the Kerala Government Rules applicable for the professional colleges.</p>
						<p>The rank score shall be arrived at as follows:</p>
						 <ul class="star size-medium">
						 	<li>Entrance test score</li>
						 	<li>Group discussion </li>
						 	<li>Personal Interview</li>
						 </ul>
						 <p>The maximum weightage for Group Discussion and personal interview for the Admission process is 20 percent.</p>
                       <h3>Contact Us</h3>
					   
					   <ul class="contact-address style1">
                            <li class="office-address">
                                <i class="fa fa-map-marker"></i>
                                <div class="details">
                                    <h5>Office Address</h5>
                                    <p>Naipunnya Business School, Pongam, Koratty East, Thrissur District, Kerala State - 680 308.</p>
                                </div>
                            </li>
                            <li class="phone">
                                <i class="fa fa-phone"></i>
                                <div class="details">
                                    <h5>Phone</h5>
                                    <p>
                                        Office: 0480 2730340, 2730341, 273357
                                        <br>
                                        Mobile: +91 9605078601
                                    </p>
                                </div>
                            </li>
                            <li class="email-address">
                                <i class="fa fa-envelope"></i>
                                <div class="details">
                                    <h5>Office Address</h5>
                                    <p>
                                        Websites: www.nbs.ac.in, www.mbanimit.ac.in

                                        <br>
                                        Email: mail@mbanimit.ac.in
                                    </p>
                                </div>
                            </li>
                        </ul>
					    
                        </div>
                    </div>
                </div>
            </div>
        </section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>