<style type="text/css">
	.manag-hed-name {
	padding-top:5px 0 5px 0;
    font-weight: 600;
	}
	.manag-hed-name small{
		font-weight:500;
	}
</style>
<?php include("inc/side-menu.php"); ?>

<!-- BANNER -SLIDER -->
<div class="page-title-container row">
	<div class="page-title">
		<div class="container">
			<h1 class="entry-title">About Us</h1>
		</div>
	</div>
	<ul class="breadcrumbs">
		<li><a href="index.php">Home</a></li>
		
		<li class="active">About Us</li>
	</ul>
</div>

</div>
<!-- HEADER -->


<!-- CONTENT-Features -->


<section id="content">
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-6">
				<div class="post-slider style1 owl-carousel box">
					<a href="#" class="soap-mfp-popup">
						<img src="images/nimit-overview.png" alt="">
					</a>
					
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<h3>History</h3>
				<p>Naipunnya Business School (NBS) is an offshoot of Naipunnya Institute of Management and Information Technology (NIMIT), Pongam, Koratty.  NIMIT is a project of the Archdiocese of Ernakulam-Angamaly. NIMIT was launched way back in 1998 under the visionary leadership of Rev. Fr. Sebastian Kalapurackal, the founder director of Naipunnya. ‘To reach the unreachable’ is the avowed mission of Naipunnya group of institutions. NBS was established in the year 2012, and since then it has continued to innovate its academics to the changes in the business environment. At the same time it encapsulates inclusion of all. NBS encompasses more than 4000 square feet, of flexible teaching and learning space.  NBS offers the very best and latest in technology for its education and thus occupies a prime position in academics, that will inspire and enable generations to become leaders in the business world.  Today Naipunnya group of educational institutions has colleges at Cherthala in the district of Alleppey and public schools at Edakkunnu and Thrikkakara in Ernakulam district.  Naipunnya group has also embarked on Civil services coaching, bank coaching and Recruitment services as well.</p>
				<h3>Milestones of NBS</h3>
				
				<ul class="star size-medium">
					<li>2014:17 students completed degree in Masters in Business Administration</li>
					<li>2015:18 students completed degree in Masters in Business Administration </li>
					<li>2016: 31 students completed degree in Masters in Business Administration  </li>
					<li>2017: 38 students completed degree in Masters in Business Administration  </li>
				</ul>
				
			</div>
		</div>
	</div>
</div>
<div class="section" id="nbs-vison">
	<div class="container">
		<div class="heading-box">
			<h2 class="box-title">Vision and Mission</h2>
			<!-- <p class="desc-lg">Get to know our values, mission and work philosophy</p> -->
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="shortcode-banner">
					<img alt="" src="images/vision.jpg">
					<div class="shortcode-banner-content">
						<h4 class="banner-title">Vision</h4>
						<div class="details">
							<p>To be a global academy, one of the world’s leading institutes that moulds students for management practices. Strive continuously for excellence in education and service to the society. 	</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<div class="shortcode-banner">
					<img alt="" src="images/mission.jpg">
					<div class="shortcode-banner-content">
						<h4 class="banner-title">Mission</h4>
						<div class="details">
							<p>To equip students with management skills so that they may function efficiently and effectively in the modern business world. To produce leaders who have an awareness and involvement in wider societal concerns. Students of NBS will experience the joy of learning, explore new horizons and excel in business arena. </p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<div class="shortcode-banner">
					<img alt="" src="images/gols.jpg">
					<div class="shortcode-banner-content">
						<h4 class="banner-title">Goals and Objectives of MBA Program</h4>
						<div class="details">
							<ul class="disc size-medium">
								<li>To develop students into business leaders ready to tackle the challenges of today's global &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;business environment</li>
								<li>To prepare students for a career in entrepreneurial skills </li>
								<li>To instill values focusing on cultural, social, and to be part of the community through &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;volunteering</li>
								<li>To prepare students to become an active member of a global society</li>
								<li>To provide opportunities to participate in activities outside the academic program.</li>
								<li>To offer an efficient mentoring support.</li>
								<li>To be change leader in changing the economic and social landscape of the century.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<div class="shortcode-banner">
					<img alt="" src="images/CoreValues.jpg">
					<div class="shortcode-banner-content">
						<h4 class="banner-title">Core Values</h4>
						<div class="details">
							<p>The mission of Naipunnya Business School is to educate students to become business leaders who make a difference in the world. For achieving this mission it requires an environment of trust and mutual respect, free expression and a commitment to truth, excellence, and lifelong learning. All our students, faculty, staff, and alumni accept are trained to accept these principles when they join NBS. This also enables them to foster values useful for the business and community. We practice these values in our daily interactions so that students are able to:</p>
							<ul class="disc size-medium">
								<li>Respect for the rights, differences, and  have a cultural immersion the larger community </li>
								<li>Practice honesty, transparency in all their dealing with members of the community</li>
								<li>To be a person who is prepared to change behavior, accept norms and be part of the &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;community  </li>
							</ul>
							
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="section" id="nbs-management">
	<div class="container">
		<div class="heading-box">
			<h2 class="box-title">Our Management</h2>
		</div>
		<div class="row">
			<div class="box">
				
				<div class="col-sm-12 text-center">
					<img class="img-thumbnail" src="images/management/Alancherry.jpg" alt="Alancherry">
					<h5 class="manag-hed-name">Cardinal Mar George Alencherry<br/>
					<small>Patron</small>
					</h5>
				</div>
				<div class="col-sm-6 text-center">
					<img class="img-thumbnail" src="images/management/Mar-Sebastian-Adayanthrath.jpg" alt="edayantharath">
					<h5 class="manag-hed-name">Bishop Mar Sebastian Adayanthrath<br/>
					<small>Co-Patron</small>
					</h5>
				</div>
				<div class="col-sm-6 text-center">
					<img class="img-thumbnail" src="images/management/Mar-Jose-Puthenveettil-2.jpg" alt="jose">
					<h5 class="manag-hed-name">Bishop Mar Jose Puthenveettil<br/>
					<small>Co-Patron</small>
					</h5>
				</div>
				<div class="col-sm-4 text-center">
					<img class="img-thumbnail" src="images/management/frsaji.jpg" alt="SAjePeter">
					<h5 class="manag-hed-name"> Fr. Saje Peter<br/>
					<small>Executive Director </small>
					</h5>
				</div>
				
				<div class="col-sm-4 text-center">
					<img class="img-thumbnail" src="images/management/FrThomas.jpg" alt="Thomas">
					<h5 class="manag-hed-name">Rev. Fr. Thomas Valookaran<br/>
					<small>Asst. Exe. Director</small>
					</h5>
				</div>
				<div class="col-sm-4 text-center">
					<img class="img-thumbnail" src="images/management/Varghese.jpg" alt="asin">
					<h5 class="manag-hed-name">Rev. Fr. Varghese Assin Thaiparambil<br/>
					<small>Asst. Exe. Director</small>
					</h5>
				</div>
				
				
				
			</div>
		</div>
		
	</div>
	
</div>

<div class="section" id="nbs-advisory">
	<div class="container">
		<div class="heading-box">
			<h2 class="box-title">Advisory Board</h2>
		</div>
		<div class="row">
			
			
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/Mar-Jose-Puthenveettil-2.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Mar Jose Puthenveettil</h4>
						<span class="team-member-job">Syncellus (Pro-Vicar General)</span>
					</div>
					
				</div>
			</div>
			
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/FrSAjePeter.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Fr. Saje Peter Kannaparamban</h4>
						<span class="team-member-job">Principal NIMIT</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/FrPaul.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Fr. (Dr.) Paulachan Kaithottungal</h4>
						<span class="team-member-job">Director, NBS</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/FrAssin.jpg" alt="">
						
					</div>
					<div class="team-member-author teamMemberImage">
						<h4 class="team-member-name">Fr. Varghese Assin Thaiparambil</h4>
						<span class="team-member-job ">Coordinator NBS</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/JacobSir.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Prof. Dr. Jacob P.M </h4>
						<span class="team-member-job">HOD, NBS</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/DrJoy.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Prof. Dr. Joy Joseph  </h4>
						<span class="team-member-job">Dean Academics, NIMIT</span>
					</div>
					
				</div>
			</div>
			
		</div>
		
		<div class="row">
			
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/SabuSir.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Prof. Sabu Varghese </h4>
						<span class="team-member-job">Assoc. Prof. NBS
						</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/coverstoryjancyjames.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Prof. Dr. Jancy James  </h4>
						<span class="team-member-job">Retd. Vice Chancellor,CUK</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/MP-Joseph-IAS.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Mr. M. P. Joseph IAS  </h4>
						<span class="team-member-job">Former Labor Commissioner</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/BennyAntony.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Dr. Benny Antony   </h4>
						<span class="team-member-job">jt. MD,<br/> Arjuna Natural</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/Dr.JoseSankoorickal.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Dr. Jose Sankoorickal  </h4>
						<span class="team-member-job">Visiting Professor, AMET University</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/joshyjoseph.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Dr. Joshy Joseph </h4>
						<span class="team-member-job">Assoc. Prof. IIM-K</span>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row">
			
			
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/Issac-Joseph-Kottukapally.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Mr. Issac Joseph  </h4>
						<span class="team-member-job">MD, Lunar Rubbers</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/Mr.KurianAbraham.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Mr. Kurian Abraham  </h4>
						<span class="team-member-job">MD, DHANAM Pvt. Ltd.</span>
					</div>
					
				</div>
			</div>
			<div class="col-sms-6 col-sm-6 col-md-2">
				<div class="team-member style-colored box">
					<div class="image-container">
						<img src="images/AdversaryBoard/Mohanachandran.jpg" alt="">
						
					</div>
					<div class="team-member-author">
						<h4 class="team-member-name">Mr. Mohanachandran</h4>
						<span class="team-member-job"> Form. GM,Federal Bank </span>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
</div>
</section>
<!--FOOTER-->



<!-- Javascript -->
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery.noconflict.js"></script>
<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Magnific Popup core JS file -->
<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- parallax -->
<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
<!-- waypoint -->
<script type="text/javascript" src="js/waypoints.min.js"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
<!-- load revolution slider scripts -->
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- plugins -->
<script type="text/javascript" src="js/jquery.plugins.js"></script>
<!-- load page Javascript -->
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/revolution-slider.js"></script>
<?php include("inc/footer.php") ?>
   <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript">
	$("#toTop").click(function () {
  
   $('html,body').animate({scrollTop: 485}, 2000);
});
</script>
</body>
</html>