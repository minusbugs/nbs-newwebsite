
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Curriculum</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">Curriculum</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<section id="content">
			<div class="container">
				<div class="row">
					
					<div id="main">
						<h3>Duration of the programme</h3>
						<p>The programme shall have four semesters. Each semester shall consist of 16 weeks. Instruction and University examinations in each course in a semester shall be completed within 90 days in a semester.</p>
						<table class="table style2">
							<thead>
								<tr>
									<th>Semester</th>
									<th>Beginning Month</th>
									<th>Closing month</th>
									<th>Duration</th>
									
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1<sup>st</sup> Semester</td>
									<td>1<sup>st</sup> August every year</td>
									<td>31<sup>st</sup> January</td>
									<td>every year 6 months</td>
								</tr>
								<tr>
									<td>2<sup>nd</sup> Semester</td>
									<td>1<sup>st</sup> February every year</td>
									<td>31<sup>st</sup> July</td>
									<td>every year 6 months</td>
								</tr>
								<tr>
									<td>3<sup>rd</sup> Semester</td>
									<td>1<sup>st</sup> August every year</td>
									<td>31<sup>st</sup> January</td>
									<td>every year 6 months</td>
								</tr>
								<tr>
									<td>4<sup>th</sup> Semester</td>
									<td>1<sup>st</sup> February every year</td>
									<td>31<sup>st</sup> July</td>
									<td>every year 6 months</td>
								</tr>
							</tbody>
						</table>
						<p>However, the
							University may condone shortage up to 10% of the maximum number of contact hours
							per semester. If the candidate has shortage more than this limit he/she has to compensate the shortage of attendance of that semester along with the next batch and
							appear for the university examination of that semester.
						</p>
						<h3>Lecture Hours</h3>
						<p>Each full course (4 Credit) shall have four hours and each half course (2 Credit) shall
							have two hours of lecture classes per semester. Each courses having a maximum of 4/2
							credits will be considered as full course and all courses having a maximum of 2 credits
							shall be considered as half courses. Each full course shall have a minimum of 60 hours of lecture and each half course 30 hours of lecture in a semester.
						</p>
						<h3>Attendance</h3>
						<p>A student shall attend at least a minimum of 75 % of the number of classes actually held for each of the courses in a semester to be eligible for appearing for university examination of that semester. If the student has shortage of attendance in a semester, he or she shall not be allowed. More information is available on: <a style="color: #14cff0;" href="http://www.universityofcalicut.info/syl/6014_2016_Admn_on11May2016.pdf">Here</a></p>

					</div>
				</div>
			</div>
			
		</section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>