
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<!-- <h1 class="entry-title">MBA HIGHLIGHTS</h1> -->
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li class="active">GRIEVANCE</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<section id="content">
			<div class="container">
				<div class="row">
					
					<div id="main">
						
						<div class="section-info">
                        <h2>STUDENTS’ GRIEVANCE REDRESSAL</h2>
                        <div class="box">
                        	<p>The students grievance redressal mechanism takes care of the complaints and difficulties of the student community. At the primary level, the teacher-in-charge will listen to the problem in detail. If it is an academic matter, the faculty member of the concerned subject is consulted to sort out the case. If the situation demands, the teacher-in-charge will also contact the parent/guardian/ hostel warden. The teacher-in-charge refers unresolved cases to the HOD. The two members can avail the services of the Student Welfare Officer if needed. At the appellate level, the Principal will redress all the unresolved cases in consultation with the College Council and the Director.</p>
                        	<h3>GRIEVANCE REDRESSAL CELL:</h3>
                        	<p>Fr. Varghese Assin – Chairperson <br/>
                        	   Mr.  Don David– Secretary<br/>
                        	   <b><u>Members:</u></b> <br/>
                        	   Mr. Antony Varghese, <br/>
                        	   Ms. Minnu Xavier, <br/>
                        	   Ms Anju Vaddakencherry  <br/>
                        	</p>
                        	<p>The members will meet at least once in every 6 months or on receipt of a grievance. The committee will review the grievance and recommend appropriate action to be taken by the principal. Students are free to handover the grievances to any of the committee members or else can drop their written grievances in the complaint placed in the front office.</p>

                        	<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSc0fWGw__tvsGHho5bpcZd5Om8tWV0aFr6kb2wZ6tnORzI6nA/viewform?embedded=true" width="100%" height="1100" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
                        </div>
                    </div>

              

					</div>
				</div>
			</div>
			
		</section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>