<link href="newsTicker/jquery.simpleTicker.css" rel="stylesheet">
<link rel="stylesheet" href="css/fontawesome1-.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="newsTicker/jquery.simpleTicker.js"></script>
<style type="text/css">
	.brand-slider .owl-item {
		width:290px !important;
		height:120px !important;
	}
	.qick-link-hed{
		font-weight:600;
	}
	.qick-link-icon-size{
		font-size:50px !important;
	}
	.galleryImg{
		height:160px !important;
	}


</style>
<script type="text/javascript">
	
	$(function(){
		$.simpleTicker($("#ticker-roll"),{'effectType':'slide'});
		$.simpleTicker($("#ticker-roll1"),{'effectType':'roll'});
	
});
</script>
<?php include("inc/side-menu.php");
include("db/dbConn.php");
$sql = "SELECT * FROM slidertext";
$result = $conn->query($sql);
$sliderArray = array();
$slider_title_1=$slider_title_2=$slider_title_3=$slider_desc_1=$slider_desc_2=$slider_desc_3="";
while($row = $result->fetch_assoc()) {
	$newdata =  array (
	'slider_title' => $row["SliderTitle"],
	'slider_desc' => $row["SliderDescription"],
	'slider_url' => $row["SliderUrl"],
	);
	array_push($sliderArray, $newdata);
}
?>



<!-- BANNER -SLIDER -->

<div class="row nbs-slider">
	<img src="images/slider/photo01.jpg" alt="" style="width:100% " id="img-dummySlider">
	<div class="post-slider style3 owl-carousel">
		<a href='<?php echo $sliderArray[0]["slider_url"] ?>' target="blank"  class="">
			<img src="images/slider/photo01.jpg" alt="">
			<div class="slide-text caption-animated" data-animation-type="slideInLeft" data-animation-duration="2">
				<h2 class="slide-title">QUALITY EDUCATION</h2>
				<!-- <p>FOR YOUR BETTER FUTURE</p> -->
			</div>
			<div class="slide-text_1 caption-animated"  data-animation-type="slideInRight" data-animation-duration="2">
				<h2 class="slide-title"><?php echo $sliderArray[0]["slider_title"] ?></h2>
				<p><?php echo $sliderArray[0]["slider_desc"] ?></p>
			</div>
			
		</a>
		<a href="<?php echo $sliderArray[1]["slider_url"] ?>" target="blank"  class="">
			<img src="images/slider/photo02.jpg" alt="">
			<div class="slide-text caption-animated" data-animation-type="slideInLeft" data-animation-duration="2">
				<h2 class="slide-title">HI-CLASS INFRASTRUCTURE</h2>
				<!-- <p>AND ITS OPTIMAL UTILIZATION</p> -->
			</div>
			<div class="slide-text_1 caption-animated"  data-animation-type="slideInRight" data-animation-duration="2">
				<h2 class="slide-title"><?php echo $sliderArray[1]["slider_title"] ?></h2>
				<p><?php echo $sliderArray[1]["slider_desc"] ?></p>
			</div>
		</a>
		<a href="<?php echo $sliderArray[2]["slider_url"] ?>" class="">
			<img src="images/slider/photo03.jpg" alt="">
			<div class="slide-text caption-animated" data-animation-type="slideInLeft" data-animation-duration="2">
				<h2 class="slide-title">INDUSTRY VISITS AND INDUSTRY INTERACTIONS</h2>
				<p></p>
			</div>
			<div class="slide-text_1 caption-animated"  data-animation-type="slideInRight" data-animation-duration="2">
				<h2 class="slide-title"><?php echo $sliderArray[2]["slider_title"] ?></h2>
				<p><?php echo $sliderArray[2]["slider_desc"] ?></p>
			</div>
		</a>
		<a href="<?php echo $sliderArray[0]["slider_url"] ?>" class="">
			<img src="images/slider/photo04.jpg" alt="">
			<div class="slide-text caption-animated" data-animation-type="slideInLeft" data-animation-duration="2">
				<h2 class="slide-title">CLASSES BY EMINENT ACADEMICIANS <br>AND CORPORATE LEADERS</h2>
				<!-- <p>AND INDUSTRY EXPERTS</p> -->
			</div>
			<div class="slide-text_1 caption-animated"  data-animation-type="slideInRight" data-animation-duration="2">
				<h2 class="slide-title"><?php echo $sliderArray[0]["slider_title"] ?></h2>
				<p><?php echo $sliderArray[0]["slider_desc"] ?></p>
			</div>
		</a>
		<a href="<?php echo $sliderArray[1]["slider_url"] ?>" class="">
			<img src="images/slider/photo05.jpg" alt="">
			<div class="slide-text caption-animated" data-animation-type="slideInLeft" data-animation-duration="2">
				<h2 class="slide-title">CREATING  BUSINESS LEADERS  OF TOMORROW</h2>
				<!-- <p>OUTBOUND TRAINING PROGRAMMES</p> -->
			</div>
			<div class="slide-text_1 caption-animated"  data-animation-type="slideInRight" data-animation-duration="2">
				<h2 class="slide-title"><?php echo $sliderArray[1]["slider_title"] ?></h2>
				<p><?php echo $sliderArray[1]["slider_desc"] ?></p>
			</div>
		</a>
		<a href="<?php echo $sliderArray[2]["slider_url"] ?>" class="">
			<img src="images/slider/photo06.jpg" alt="">
			<div class="slide-text caption-animated" data-animation-type="slideInLeft" data-animation-duration="2">
				<h2 class="slide-title">A NEW GENERATION OF ENTREPRENEURS</h2>
				<!-- <p>OF ENTREPRENEURS</p> -->
			</div>
				<div class="slide-text_1 caption-animated"  data-animation-type="slideInRight" data-animation-duration="2">
				<h2 class="slide-title"><?php echo $sliderArray[2]["slider_title"] ?></h2>
				<p><?php echo $sliderArray[2]["slider_desc"] ?></p>
			</div>
		</a>
		<a href="<?php echo $sliderArray[0]["slider_url"] ?>" class="">
			<img src="images/slider/photo07.jpg" alt="">
			<div class="slide-text caption-animated" data-animation-type="slideInLeft" data-animation-duration="2">
				<h2 class="slide-title">TRAINING FOR CORPORATE PREPAREDNESS </h2>
				<!-- <p>AND TRAINING FOR PLACEMENTS</p> -->
			</div>
			<div class="slide-text_1 caption-animated"  data-animation-type="slideInRight" data-animation-duration="2">
				<h2 class="slide-title"><?php echo $sliderArray[0]["slider_title"] ?></h2>
				<p><?php echo $sliderArray[0]["slider_desc"] ?></p>
			</div>
		</a>
	</div>
	
	
</div>
<div class="row news-div">
	
	<div class="col-md-2 text-right">
		<h3>News & Events :</h3>
	</div>
	<div class="col-md-10">
		<!-- <p><span class="highlight">Admission</span>Admission started for the academic year 2018-2019</p> -->
		<div id="ticker-roll" class="ticker highlight">
			<ul>
				<?php
					include("db/dbConn.php");
					$sql = "SELECT * FROM events";
					$result = $conn->query($sql);
					
					while($row = $result->fetch_assoc()) {
						echo "<li>".$row["EventName"]."</li>";
					}
				?>
				
			</ul>
			</div><!--/#ticker -->
		</div>
	</div>
	
	
</div>
<!-- HEADER -->
<hr class="color-light col-sm-8">
<!-- CONTENT-Features -->
<section class="container-fluid">
	<div class="section-info">
		<div class="heading-box">
			<h2 class="box-title section-heading">MBA PROGRAM  <span class="font-color-yellow">HIGHLIGHTS</span></h2>
			
		</div>
		<div class="row">
			<div class="col-sm-3">
				<a href="mba-program.php#nbs-course-features">
					<div class="icon-box style-boxed-1 icon-color-blue">
						<div class="icon-container">
							<i class="fa fa-lightbulb-o"></i>
							<!-- <img src="images/img2.png"> -->
						</div>
						<div class="box-content">
							<h4 class="box-title"><a href="mba-program.php#nbs-course-features" class="mba-prgm-a">Course features</a></h4>
						</div>
					</div>
				</a>
				<br>
			</div>
			<div class="col-sm-3">
				<a href="mba-program.php#nbs-placement">
					<div class="icon-box style-boxed-1 icon-color-blue">
						<div class="icon-container">
							<i class="fa fa-lightbulb-o"></i>
						</div>
						<div class="box-content">
							<h4 class="box-title"><a href="mba-program.php#nbs-placement" class="mba-prgm-a">Placement support</a></h4>
						</div>
					</div>
				</a>
				<br>
			</div>
			<div class="col-sm-3">
				<a href="mba-program.php#nbs-accreditations">
					<div class="icon-box style-boxed-1 icon-color-blue">
						<div class="icon-container">
							<i class="fa fa-lightbulb-o"></i>
						</div>
						<div class="box-content">
							<h4 class="box-title"><a href="mba-program.php#nbs-accreditations" class="mba-prgm-a">Accreditations & Industry partners</a></h4>
						</div>
					</div>
				</a>
				<br>
			</div>
			<div class="col-sm-3">
				<a href="mba-program.php#nbs-Industrial">
					<div class="icon-box style-boxed-1 icon-color-blue">
						<div class="icon-container">
							<i class="fa fa-lightbulb-o"></i>
						</div>
						<div class="box-content">
							<h4 class="box-title"><a href="mba-program.php#nbs-Industrial" class="mba-prgm-a">Industry Interactions</a></h4>
						</div>
					</div>
				</a>
				<br>
			</div>
		</div>
	</div>
</section>
<div class="callout-box style2">
	<div class="container">
		<div class="callout-content">
			<div class="callout-text">
				
			</div>
			<div class="callout-action">
				
			</div>
		</div>
	</div>
</div>
<!-- Content Course -->
<section class="container-fluid">
	<div class="section-info">
		<div class="heading-box">
			<h2 class="box-title section-heading">MBA <span class="font-color-yellow">SPECIALIZATION</span></h2>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-4">
			<div class="shortcode-banner style-animated" data-animation-type="rotateInUpLeft" data-animation-delay="0">
				<img src="images/courses/1.jpg" alt="">
				<div class="shortcode-banner-inside">
					<div class="shortcode-banner-content">
						<h3 class="banner-title">Marketing </h3>
						<div class="details font-color-black">
							<p>An intensive immersion course designed to develop skills in formulating and implementing marketing strategies for brands and businesses.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-4">
			<div class="shortcode-banner style-animated" data-animation-type="rotateInUpLeft" data-animation-delay="0">
				<img src="images/courses/2.jpg" alt="">
				<div class="shortcode-banner-inside">
					<div class="shortcode-banner-content">
						<h3 class="banner-title">Finance</h3>
						<div class="details font-color-black">
							<p>A course which offers opportunity to learn financial accounting, commercial banking, investment banking, financial planning, money management, and behavioral finance.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-4">
			<div class="shortcode-banner style-animated" data-animation-type="rotateInUpLeft" data-animation-delay="0">
				<img src="images/courses/3.jpg" alt="">
				<div class="shortcode-banner-inside">
					<div class="shortcode-banner-content">
						<h3 class="banner-title">Human Resource</h3>
						<div class="details">
							<p>Creating the HR professional to be a strategic partner in managing today’s organizations through, recruitment, selection, development, appraisal, performance management, retention, compensation, and labor relations.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>
<!-- Content Admission Procedure -->
<section class="container-fluid">
	<div class="row">
		<div class="col-md-4"><hr class="color-heading col-md-4"></div>
		
		<div class="col-md-4">
			<h2 class="box-title section-heading-2">ADMISSION <span class="font-color-yellow">PROCEDURE</span></h2>
		</div>
		<div class="col-md-4"><hr class="color-heading col-md-4"></div>
	</div>
	<div class="section-info">
		<div class="row">
			<div class="col-md-offset-2">
				<ul class="process-builder style-creative clearfix same-height">
					<li class="process-item">
						<div class="process-inside" data-animation-type="fadeInLeft" data-animation-delay="0" data-animation-duration="1.5">
							<a href="Attachments/MBABrochure.pdf" target="blank">
								<div class="process-image">
									<img src="images/process-1.png" alt="">
								</div>
							</a>
							<div class="process-details">
								<h4 class="process-title font-color-orange"><a href="Attachments/MBABrochure.pdf" target="blank">View Prospectus</a></h4>
							</div>
						</div>
						
					</li>
					<li class="process-item">
						<div class="process-inside" data-animation-type="fadeInLeft" data-animation-delay="1" data-animation-duration="2">
							<a href="Feestructure.php">
								<div class="process-image">
									<img src="images/process-2.png" alt="">
								</div>
							</a>
							<div class="process-details">
								<h4 class="process-title font-color-orange"><a href="Feestructure.php">Fees, Scholarships & Loans</a></h4>
							</div>
						</div>
						
					</li>
					<li class="process-item">
						<div class="process-inside" data-animation-type="fadeInLeft" data-animation-delay="2" data-animation-duration="2">
							<a href="enquiry.php">
								<div class="process-image">
									<img src="images/process-3.png" alt="">
								</div>
							</a>
							<div class="process-details">
								<h4 class="process-title font-color-orange"><a href="enquiry.php">Enquiry Form</a></h4>
							</div>
						</div>
						
					</li>
					
					<li class="process-item">
						<div class="process-inside" data-animation-type="fadeInLeft" data-animation-delay="2" data-animation-duration="2">
							<a href="contact.php">
								<div class="process-image">
									<img src="images/process-4.png" alt="">
								</div>
							</a>
							<div class="process-details">
								<h4 class="process-title font-color-orange"><a href="contact.php">Contact</a> </h4>
							</div>
						</div>
						
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<hr class="color-heading col-md-12">
</section>
<!-- news and evnts secction -->
<section class="container-fluid background-color">
	<div class="row">
		<div class="col-md-4">
			<div class="row">
				<div class="news-head row">
					<div class="col-md-2">
						<img src="images/news.png" alt="Qick Connect">
					</div>
					<div class="col-md-10">
						<h2 class="section-heading-2">News & Events</h2>
					</div>
				</div>
			</div>
			<hr class="color-heading-grey col-md-12">
			<div class="blog-posts">
				<article class="post post-full post-blockquote">
					<div class="post-content">
						
						<blockquote class="style1">
							<div id="ticker-roll1" class="ticker highlight">
								<ul>
									<?php
										include("db/dbConn.php");
										$sql = "SELECT * FROM events";
										$result = $conn->query($sql);
										
										while($row = $result->fetch_assoc()) {
											echo "<li><p>".$row["EventName"]."</p></li>";
										}
									?>
									
								</ul>
								</div><!--/#ticker -->
								<!-- <p>Duis eget ultricies lorem, et rhoncus augue. Aliquam id est semper, tincidunt nisi ac, tristique enim. Phasellus accumsan, enim eget .</p> -->
							</blockquote>
							
						</div>
					</article>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="news-head row">
						<div class="col-md-2">
							<img src="images/video.png" alt="Qick Connect">
						</div>
						<div class="col-md-10">
							<h2 class="section-heading-2">Video Gallery</h2>
						</div>
					</div>
					<hr class="color-heading-grey col-md-12">
				</div>
				<!-- <div class="row"> -->
				<div class="blog-posts">
					<article class="post post-full post-blockquote">
						<div class="post-content">
							<iframe  src="https://www.youtube.com/embed/ctTNfabsmTI" style="width:100%" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
						</div>
					</article>
				</div>
				<!-- </div> -->
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="news-head row">
						<div class="col-md-2">
							<img src="images/camera.png" alt="Qick Connect">
						</div>
						<div class="col-md-10">
							<h2 class="section-heading-2">Image Gallery</h2>
						</div>
					</div>
				</div>
				<hr class="color-heading-grey col-md-12">
				
				<article class="post post-masonry">
					
					<div class="post-content">
						<div class="post-slideshow owl-carousel">
							<?php
							$sql = "SELECT * FROM gallery where GalleryId=0";
										$result = $conn->query($sql);
										
										while($row = $result->fetch_assoc()) {
											$path=$row["ImagePath"];
							?>
							<div class="image hover-style2">
								<img src="http://admin.nbs.ac.in/assets/Gallery/<?php echo $path; ?>" alt="" class="galleryImg">
								
							</div>
							<?php
						}?>
							
						</div>
					</div>
					
				</article>
				
			</div>
		</div>
	</section>
	<!-- Testimonial Section -->
	<section class="container-fluid background-color-blue">
		<div class="section-info">
			<div class="row">
				<div class="news-head row">
					<div class="col-md-1">
						<img src="images/testimonil.png" alt="Qick Connect">
					</div>
					<div class="col-md-10">
						<h2 class="section-heading-3">Testimonials</h2>
					</div>
				</div>
			</div>
			<hr class="color-heading-grey col-md-12">
			<div class="col-md-12">
				<div class="testimonials style3 owl-carousel box-lg" data-transitionstyle="fade">
					<div class="testimonial style4">
						<div class="testimonial-image">
							<img src="images/Testimonials/Jovish.jpg" alt="">
						</div>
						<div class="testimonial-content">
							The best thing happened to me during two years of MBA was that I learned a lot about myself. NBS helped me to develop very positive attitude towards work and also developed leadership quality in me
						</div>
						<div class="testimonial-author">
							<span class="testimonial-author-name">Mr. Jovish Jose</span> - <span class="testimonial-author-job">Management Trainee –Sales & Operations ,AIS Group
						</span>
					</div>
				</div>
				<div class="testimonial style4">
					<div class="testimonial-image">
						<img src="images/Testimonials/Jijo.jpg" alt="">
					</div>
					<div class="testimonial-content">
						Domain nourishment and instillation of moral values are two foremost fundamentals of this institute
					</div>
					<div class="testimonial-author">
						<span class="testimonial-author-name">Mr.Jijo Janardhanan</span> - <span class="testimonial-author-job">Associate, KPMG</span>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>
<div class="parallax has-caption parallax-image2" data-stellar-background-ratio="0.5">
	<div class="caption-wrapper">
		<h2 class="caption animated size-lg" data-animation-type="fadeInLeft" data-animation-duration="2" data-animation-delay="0"><a href="mbalife.php">WHY CHOOSE US?</a></h2>
		<br>
		<h3 class="caption animated size-md" data-animation-type="fadeInLeft" data-animation-duration="2" data-animation-delay="1"><a href="mbalife.php">Life @ NBS</a></h3>
	</div>
</div>
<!--Quick Links -->
<div class="section-info">
	<div class="heading-box">
		<h2 class="box-title section-heading">Quick <span class="font-color-yellow">Links</span></h2>
		
	</div>
	<div class="row">
		
		<div class="col-sm-4 col-md-2">
			<div class="icon-box style-centered-1 box">
				<i class="fa fa-sign-in qick-link-icon-size"></i>
				<h4 class="box-title qick-link-hed"><a href="http://117.211.168.147/">FACULTY LOG IN</a></h4>
				<div class="box-content">
					
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-2">
			<div class="icon-box style-centered-1 box">
				<i class="fa fa-credit-card qick-link-icon-size"></i>
				<h4 class="box-title qick-link-hed"><a href="https://epay.federalbank.co.in/easypayments/">PAY FEE ONLINE</a></h4>
				<div class="box-content">
					
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-2">
			<div class="icon-box style-centered-1 box">
				<i class="fa fa-file-text qick-link-icon-size"></i>
				<h4 class="box-title qick-link-hed"><a href="Attachments/e-application.pdf">MBA APPLICATION FORM</a></h4>
				<div class="box-content">
					
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-2">
			<div class="icon-box style-centered-1 box">
				<i class="fa fa-book qick-link-icon-size"></i>
				<h4 class="box-title qick-link-hed"><a href="https://jgateplus.com/home/">ONLINE LIBRARY</a></h4>
				<div class="box-content">
					
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-2">
			<div class="icon-box style-centered-1 box">
				<i class="fa fa-flag qick-link-icon-size"></i>
				<h4 class="box-title qick-link-hed"><a href="http://nimitexpert:8087/Staff/Login.aspx">MENTOR MATE</a></h4>
				<div class="box-content">
					
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-2">
			<div class="icon-box style-centered-1 box">
				<i class="fa fa-briefcase qick-link-icon-size"></i>
				<h4 class="box-title qick-link-hed"><a href="https://docs.google.com/forms/d/1Cjij0EpnWR5OgUHFGwRGCfeCkhXaOBY1rzwb61aNtOw/viewform?edit_requested=true">PLACEMENT REGISTRATION</a></h4>
				<div class="box-content">
					
				</div>
			</div>
		</div>
		
		
	</div>
</div>
<!-- Our Clients  -->
<section class="container-fluid" style="background-color: #ecf9f9;">
	<div class="section-info">
		<div class="row">
			<div class="heading-box">
				<h2 class="box-title section-heading">Major <span class="font-color-yellow">Recruiters</span></h2>
				
			</div>
			
			<div class="overflow-hidden">
				<div class="brand-slider owl-carousel" data-items="4" data-itemsPerDisplayWidth="[[0, 1], [480, 1], [768, 2], [992, 3], [1200, 4]]">
					<a href="#">
						<img src="images/clients/1.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/2.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/3.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/4.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/5.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/6.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/7.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/8.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/9.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/10.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/11.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/12.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/13.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/14.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/15.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/16.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/17.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/18.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/19.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/20.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/21.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/22.png" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/23.png" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/24.jpg" alt="Client Logo">
					</a>
					
					<a href="#">
						<img src="images/clients/26.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/27.png" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/28.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/29.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/30.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/31.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/32.png" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/33.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/34.png" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/35.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/36.jpg" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/37.png" alt="Client Logo">
					</a>
					<a href="#">
						<img src="images/clients/38.jpg" alt="Client Logo">
					</a>
					
				</div>
			</div>
			
		</div>
		
		
		
		
	</div>
</section>
<!--FOOTER-->
<!-- Javascript -->
<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery.noconflict.js"></script>
<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Magnific Popup core JS file -->
<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- parallax -->
<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
<!-- waypoint -->
<script type="text/javascript" src="js/waypoints.min.js"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="components/owl-carousel/owl.carousel.js"></script>
<!-- load revolution slider scripts -->
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- plugins -->
<script type="text/javascript" src="js/jquery.plugins.js"></script>
<!-- load page Javascript -->
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/revolution-slider.js"></script>
<?php include("inc/footer.php") ?>
</body>
</html>