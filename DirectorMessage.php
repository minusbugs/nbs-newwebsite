
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Director , NBS message</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li class="active">Director , NBS message</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		
		
		<!-- CONTENT-Features -->
		<div class="single-post">
			<section id="content">
				<div class="container">
					<div id="main">
						<article class="post box-lg">
							
							<div class="about-author box">
								<div class="author-img">
									<span><img src="" alt=""></span>
								</div>
								<div class="about-author-content">
									<div class="social-icons">
										<a href="#" class="social-icon"><i class="fa fa-twitter has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-facebook has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-google-plus has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-skype has-circle"></i></a>
										<a href="#" class="social-icon"><i class="fa fa-tumblr has-circle"></i></a>
									</div>
									
									<span class="nbs-author-name">FR. DR. PAUL KAITHOTTUNGAL</span> - <span class="nbs-author-desc">Director </span>
									<p>Naipunnya Business School (NBS) offers an integrated, holistic, structured and industry specific, MBA programme which gives students the vital edge for securing an excellent professional career. </p><p>
The mission of NBS is to groom students for taking up leadership positions in business organizations and to excel as business visionaries of tomorrow. Throughout this programme we continually seek to achieve NBS vision by imparting academic excellence, improving corporate excellence, instilling character excellence, and inspiring value excellence. </p><p>
Our inbound and outbound training programs grooms students to become  the most sought out leaders in the global marketplace. We also mentor and nurture them to become thoroughbred professionals, who take up the challenges of the modern business world.  This is done by stimulating the mind for creative and innovative thinking.  Students are motivated to create businesses in the campus for developing the spirit of  entrepreneurship. Over the years the research skills of our faculty have facilitated in coming out with outstanding contributions in business concepts and thinking. </p>

</p>
								</div>
							</div>
							
						</article>
						
					</div>
				</div>
			</section>
		</div>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>