-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2018 at 03:53 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nbsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `EventId` int(11) NOT NULL,
  `EventName` varchar(250) NOT NULL,
  `EventDescription` text NOT NULL,
  `EventDate` date NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`EventId`, `EventName`, `EventDescription`, `EventDate`, `CreatedOn`, `UpdatedOn`) VALUES
(1, 'Admission started for the academic year 2018-2019', 'Admission started for the academic year 2018-2019', '2018-03-05', '2018-03-08 10:40:41', '2018-03-08 10:40:41'),
(2, 'Vaidakthya’18, National Conference on Contemporary Business Environment', 'Vaidakthya’18, National Conference on Contemporary Business Environment: Changes &amp;\r\nChallenges on March 20, 2018', '0000-00-00', '2018-03-08 11:16:49', '2018-03-08 11:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `FacultyId` int(11) NOT NULL,
  `FacultyName` varchar(50) NOT NULL,
  `AboutFaculty` text NOT NULL,
  `FacultyImage` varchar(100) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`FacultyId`, `FacultyName`, `AboutFaculty`, `FacultyImage`, `CreatedOn`, `UpdatedOn`) VALUES
(1, 'Fr. Dr. Paul Kaithottungal, Director', 'Fr. (Dr.) Paul Kaithottungal is a catholic priest ordained for the Archdiocese of Ernakulam Angamaly whose passion for learning and teaching has earned accolades and acceptance across the country.  After completing his BA (Economics, Sociology and History) program from University of Mysore, Fr. Paul studied theology at St Joseph’s Seminary, Mangalore, certified by Urbaniana University, Rome. However his interests in economics and marketing led him to pursue the MBA program at Rajagiri Centre for Business Studies during 2000-2002.  It is his desire to master intricacies and nuances of economics and marketing that made him pursue M.Phil from Bharathidasan University in 2007, and Phd in marketing from D. Y. Patil University, Navi Mumbai in 2014. He has a deep passion for organizational culture, organizational performance, cross-cultural buying behavior, social change, advertising, and strategic communication.  He completed his Faculty Development Program (FDP) from IIM, Ahmedabad in 2006 had been a faculty with D. Y. Patil University.  He is actively involved in academic development programs and teaches managerial economics. He is one of the outstanding faculty members who can bridge economics with marketing. His publications include Influence of organizational culture on social acceptability, Organizational performance on social acceptability, Religiosity in Buying Decision making, and Impulsive buying behavior. He is also a keynote speaker and consultant to several organizations for marketing decisions, and an invited speaker by many companies and organizations.  Fr. (Dr.) Paul academic leadership has been the transformational driving force for several institutions under Naipunnya Group. He has introduced Faculty Enrichment Programs for fostering staff development, has been addressing different teaching and learning styles in academic programs of Naipunnya educational institutions. He had coordinated ISO activities for Naipunnya Institute of Management and Information Technology (NIMIT) to get the institute ISO certified.  He is a warm, loyal, talented and caring person who loves making a difference in the life of everyone who comes across.\r\n', 'frpaul.jpg', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(2, 'Prof. (Dr.) Jacob Mathew, HoD', 'A corporate trainer and Sales manager, Prof. Jacob is a seasoned professional with 20 years of experience in the corporate. A true professional with a passion for excellence, Prof. Jacob started of his career as a sales executive in Glaxosmithkline India Ltd. During his career he has served in various positions in Sales, Marketing, and training. After his long stint in the corporate, Prof. Jacob took up studies in the prestigious Assumption university of Thailand and graduated in Psychology.  Later he worked as a counselor in students department. Returning to India he pursued, MBA and PhD in Human Resource Management from Annamalai University.  He has published 25 papers in various area of management. He has also participated in various conferences and seminars and organized workshops and conferences in India and abroad.  Apart from being a faculty, he has been a trainer in soft skills, career and psychological inventories. His areas of interest include Social networking sites, Performance Management, Organizational Behavior, Psychological testing and Corporate Social Responsibility. Prof. Jacob has also been involved in career counseling and has been associated with admissions, and placement. He has also travelled widely across the globe to several countries in Europe, Southeast Asia, Middle East, Israel, Palestine,  UK and Australia.', 'Jacob25th July.jpg', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(3, 'Prof. Sabu Varghese, Associate Professor', 'Mr. Sabu has approximately 20 years of industry experience and 8 years in academics. His accomplished management career reflects more than 10 years of experience in operational leadership, resource utilization and organizational development. He is an innovative training and development professional with experience in training employees for supervisory skills and organizational effectiveness. He is the training manager and the management representative - ISO for Naipunnya Institute of Management & Information Technology. He is currently pursuing PhD in Human Resource domain at KUFOS. He is a resource person for many training and development programs. Mr. Sabu’s academic qualifications include M Sc. and MBA. His interested areas are organizational development, leadership training and development etc.\r\n', 'SabuVarghese.jpg', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(4, 'Prof.  Nijo Varghese, Assistant Professor', 'Mr. Nijo has his post graduation in MBA (Marketing, Finance) and has 7 years of industry experience and 2 years of academic experience. A scholar who has proved his academic aspirations in the wide array of subjects ranging from information technology to brand management. His portfolio of domains demands continuous learning and keen observation of biz world. Currently, Prof. Nijo is pursuing PhD in marketing at Kerala University of Fisheries and Oceanographic Sciences (KUFOS).  \r\n', 'NijoVarghese.jpg', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(5, 'Prof. (Mrs.) Ayana Johny, Assistant Professor', 'Dr. (Mrs). Ayana Johny, a professional with four years of corporate experience and two years of teaching experience. Started her career as Assistant Manager with South Indian Bank Ltd. During the banking career she worked in various positions in marketing, Information technology department and was the faculty in staff training college of the bank. Worked as faculty in Xavier Institute of Management and Entrepreneurship (XIME, Kochi). She is also a resource person in Employment Guidance Bureau, Cochin University of Science and technology. She was awarded Ph.D in Information Technology and Banking by Cochin University of Science and Technology. She has two international publications and three paper presentations to the credit. Her academic qualifications include MBA (Finance) and Post Graduate Diploma in Teaching and Research in Management. Her areas of interest are banking and case research methods.', 'MrsAyana.jpg', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(6, 'Asha Antony', 'Asha Antony Faculty in Finance, possess MPhil in research and development from Gandhi gram Rural University, MBA from ICFAI University, M.Com from Annamalai University and is pursuing her doctoral program at Mother Teresa Women\'s University. She has 8 yrs of exposure in shipping with freight forwarders in mumbai  as documentation officer and 9 years of educational experience in teaching with different B-schools in tamilnadu and kerala. Her area of interest is micro finance and international business. ', '', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(7, 'Prof. Jithin Benedict', 'Assistant Professor: Mr. Jithin Benedict is an MBA (Marketing and Systems) from Rajagiri School of Business studies and MSc in Psychology. He has 5 years of industry experience in Infosys Technologies Ltd. and Idea Cellular Ltd. as Asst. Manager. He is currently pursuing his PhD in marketing domain at KUFOS. His area of interest comprises Retail analytics, Brand Management and other realms of Marketing and Information Technology. He is a professional trainer and keen in equipping students with skills for B- World.\r\n', 'JB.JPG', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(8, 'Prof. A. Bhuvanes Kumar', 'Assistant Professor is an B.Tech., MBA (Finance & Marketing) from Anna University, Chennai and a University rank holder in Business Studies. He has also qualified UGC-NET. Being a topper in MBA, he got a Singapore internship sponsored by the college management for the period of 45 days. He completed his B.Tech from PSG College of Technology, Coimbatore and served Voltas Ltd (TMD) as a Project Engineer for the period of 3 years and he has been serving in the teaching field for the past six years. He has participated in various national and international conferences, workshops and seminars in India. His area of interest comprises Financial Management, Financial modeling, Operations, Costing, Quantitative Techniques and Banking. He is planning to register for PhD shortly.', 'bhuvanes.JPG', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(9, 'Adv. Abi Antony K', 'Business Law, Assistant Professor completed his LLB from Mahatma Gandhi University, specializing in Labor law in 1995. He holds a business degree from Madurai Kamaraj University specializing in Human Resource Management, and PG diplomas in Journalism as well as Industrial Relations and Personnel Management. He is a member of the Kerala High Court Advocates Association, and Ernakulam District Court Bar Association. A visiting faculty at NBS, Prof. Abi Antony\'s areas of interest includes Constitutional Laws, Intellectual Property Rights, Banking and Financial laws, Environmental Laws, Business Laws and Consumer laws. He also has 2 decades of experience encompassing advocacy, teaching, training and research.\r\n', 'MrAbi.JPG', '2018-03-08 10:41:58', '2018-03-08 10:41:58'),
(10, 'Fr. Jose Koluthuvellil', ' Fr. Jose Koluthuvallil is the Asst. Executive Director and coordinator NBS. He was ordained as a catholic priest in 2008. He had his studies at the minor seminary, Thrikkakara during 1998-00, Philosophy at Thrissur during 2000-03, and Theology  at Vadavathoor during 2004-07. Later he did his MBA program at Naipunnya Business School in 2016. Currently he is a faculty with NBS teaching Organizational Behavior, HR and Marketing.', '', '2018-03-08 10:41:58', '2018-03-08 10:41:58');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `ImageId` int(11) NOT NULL,
  `ImagePath` varchar(250) NOT NULL,
  `ImageName` varchar(100) NOT NULL,
  `GalleryId` int(11) NOT NULL DEFAULT '0',
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UpdatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`ImageId`, `ImagePath`, `ImageName`, `GalleryId`, `CreatedOn`, `UpdatedOn`) VALUES
(1, '1.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(2, '2.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(3, '3.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(4, '4.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(5, '5.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(6, '6.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(7, '7.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(8, '8.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(9, '9.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(10, '10.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(11, '11.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(12, '12.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(13, '13.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(14, '14.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(15, '15.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(16, '16.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(17, '17.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(18, '18.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(19, '19.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(20, '20.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(21, '21.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(22, '22.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(23, '23.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(24, '24.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(25, '25.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(26, '26.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(27, '27.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(28, '28.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06'),
(29, '29.JPG', 'Naipunnya Images', 0, '2018-03-08 14:03:06', '2018-03-08 14:03:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`EventId`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`FacultyId`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`ImageId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `EventId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `FacultyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `ImageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
