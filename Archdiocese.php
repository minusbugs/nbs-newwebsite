
			<?php include("inc/side-menu.php"); ?>
			<!-- BANNER -SLIDER -->
			<div class="page-title-container row">
				<div class="page-title">
					<div class="container">
						<h1 class="entry-title">Archdiocese</h1>
					</div>
				</div>
				<ul class="breadcrumbs">
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li class="active">Ernakulam-Angamaly Archdiocese</li>
				</ul>
			</div>
			
		</div>
		<!-- HEADER -->
		<!-- CONTENT-Features -->
		<section id="content">
            <div class="container">
                <div class="row">
                    
                    <div id="main" class="col-sm-12">
                        <div class="blog-posts">
                            <article class="post post-full">
                                <div class="post-image col-md-2">
                                    <div class="image">
                                        <img src="http://mbanimit.ac.in/Contents/img/ekm-archdiocese.jpg" alt="">
                                        <div class="image-extras">
                                            <a href="#" class="post-gallery"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-content col-md-10">
                                    
                                    <p>The Archdiocese of Ernakulam-Angamaly (formerly known as Archdiocese of Ernakulam) occupies a prominent position in the history of the Syro-Malabar Church. It was established as a Vicariate by the Bull "Quae Rei Sacrae" of Pope Leo XIII on 28 July 1896. Mar Aloysius Pazheparambil was appointed as the first Vicar Apostolic, who assumed charge on 5th November 1896.</p>
                                    <p>Under the able guidance of Mar Pazheparambil, the Vicariate began to flourish. City of Ernakulam was chosen to be the location for the Bishop's House which was completed and blessed on 24th April 1900. Although he had to begin from the scratches, his indefatigable labors, coupled with the hearty co-operation of the clergy and laity brought the Vicariate within a few years, to a position of eminence. He continued to govern the Vicariate ably and successfully till his death on 9th December 1919. The progress of the Vicariate both spiritually and materially was accelerated ever since Mar Augustine Kandathil assumed the charge of it on 18th December 1919. There are a number of churches, monasteries, convents, seminaries, schools, colleges, technical institutions, printing presses, and hospitals, homes for retired priests, social centers and various associations and institutes dedicated to intellectual and vocational apostolates. His Holiness Pope Benedict XVI nominated Mar George Alencherry a member of the College of Cardinals on 6th January 2012. At the consistory on 18th February 2012 he was raised to the dignity of a cardinal. The Archdiocese of Ernakulam-Angamaly with its headquarters in the Corporation of Cochin comprises of almost the whole of the district of Ernakulam, parts of the districts of Thrissur, Kottayam and Alleppey</p>
                                    
                                </div>
                            </article>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
		
		
		<!--FOOTER-->
		<?php include("inc/footer.php") ?>
		<!-- Javascript -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="js/jquery.noconflict.js"></script>
		<script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>
		<!-- Twitter Bootstrap -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- parallax -->
		<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
		<!-- waypoint -->
		<script type="text/javascript" src="js/waypoints.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="components/owl-carousel/owl.carousel.min.js"></script>
		<!-- load revolution slider scripts -->
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
		<!-- plugins -->
		<script type="text/javascript" src="js/jquery.plugins.js"></script>
		<!-- load page Javascript -->
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/revolution-slider.js"></script>
	</body>
</html>